package com.huynv.dentcareWebApi.Config;

import com.huynv.dentcareWebApi.contants.Constants;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * TODO: write you class description here
 *
 * @author
 */

@Configuration
@EnableWebMvc           //dùng Spring MVC model view controller
public class RestApiConfiguration implements WebMvcConfigurer {         //cho phép tùy chỉnh config

    @Override
    public void addCorsMappings(CorsRegistry registry) {        //CORS Cross Origin Resource Sharing
        registry
                .addMapping("/**")
                .allowedOrigins(Constants.webAddress.split("\\s*,\\s*"))
                .allowedMethods("GET", "HEAD", "POST", "PUT", "PATCH", "DELETE")
                .allowedHeaders("Origin", "Access-Control-Allow-Origin",
                        "Access-Control-Allow-Headers", "Content-Type", "Authorization", "X-Requested-With",
                        "Accept", "X-Requested-With", "remember-me", "authorization", "x-auth-token",
                        "reportProgress", "Access-Control-Request-Method", "Access-Control-Request-Headers")
                .allowCredentials(false)
                .maxAge(3600L)
        ;

    }



}
