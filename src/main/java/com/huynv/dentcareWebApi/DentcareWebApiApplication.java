package com.huynv.dentcareWebApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DentcareWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DentcareWebApiApplication.class, args);
	}

}
