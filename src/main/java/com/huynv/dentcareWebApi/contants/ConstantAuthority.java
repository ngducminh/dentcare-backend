package com.huynv.dentcareWebApi.contants;

public class ConstantAuthority {
    //quán lý tài khoản
    public interface Account {
        String MANAGE = "ACCOUNT_MANAGE";
        String VIEW = "ACCOUNT_VIEW";
        String CREATE = "ACCOUNT_CREATE";
        String UPDATE = "ACCOUNT_UPDATE";
        String DELETE = "ACCOUNT_DELETE";
    }

    //quản lý danh mục
    public interface Category {
        String MANAGE = "CATEGORY_MANAGE";
        String VIEW = "CATEGORY_VIEW";
        String CREATE = "CATEGORY_CREATE";
        String UPDATE = "CATEGORY_UPDATE";
        String DELETE = "CATEGORY_DELETE";
    }
}
