package com.huynv.dentcareWebApi.contants;

/**
 * TODO: write you class description here
 *
 * @author
 */

public class Constants {



  public static final String webAddress = ConfigProperties.getProperty("wb.address");

  public static final String ckfinder_baseDir = ConfigProperties.getProperty("ckfinder.baseDir");



  /* spring không hỗ trợ sét defautl ngôn ngữ cho @Valid */
  public static final String NOTE_API = "Truyền header: Accept-Language để response trả về lỗi theo ngôn ngữ chỉ định, hỗ trợ 2 ngôn ngữ (vi, en), mặc định ngôn ngữ hệ thống \n";
  public static final String NOTE_API_PAGEABLE = "( lưu ý param truyền lên phải encode ) \n";
  public static final String API_KEY = "JWT Authentication";
  public static final String AUTHORIZATION = "Authorization";
  public static final String HEADER = "header";
  public static final String[] WEB_IGNORING = {
      // -- swagger ui
      "/swagger-resources/**",
      "/swagger-ui.html**",
          "/swagger-ui/index.html",
      "/swagger-ui/**",
      "/v2/api-docs/**",
      "/webjars/**",
      "/public/**"
  };

    public interface STATUS {
      Long LOCK = 1L;
      Long ACTIVE = 0L;
    }

    public interface GENDER {
      Long FEMALE = 1L;
      Long MALE = 0L;
    }

    public interface IS_DELETE {
        Long DELETED = 1L;
        Long NOT_DELETED = 0L;
    }

  public interface EQUIPMENT_TYPE {
    Long THIET_BI_CO_BAN = 0L;
    Long DUNG_CU_KHAM = 1L;
    Long GHE_NHA_KHOA = 2L;
    Long THIET_BI_KHU_TRUNG = 3L;
    Long THIET_BI_KIEM_TRA = 4L;
    Long THIET_BI_CAY_GHEP = 5L;
    Long THIET_BI_THAM_MY = 6L;
  }

  public interface SERVICE_TYPE {
    Long TU_VAN = 0L;
    Long THAM_MY_RANG = 1L;
    Long CAY_GHEP = 2L;
    Long TIEU_PHAU_THU_THUAT = 3L;
  }

  public interface PERMISSION_TYPE {
    Long ADMIN = 0L;
    Long DOCTOR = 1L;
    Long STAFF = 2L;
    Long CUSTOMER = 3L;
  }

  public interface APPOINTMENT_TYPE {
    Long CHO_DUYET = 0L;
    Long DA_DUYET = 1L;
    Long DANG_KHAM = 2L;
    Long DA_KHAM = 3L;
    Long HUY_LICH = 4L;

    static String getAppointmentType(Long status) {
        switch (status.intValue()) {
            case 0:
            return "Chờ duyệt";
            case 1:
            return "Đã duyệt";
            case 2:
            return "Đang khám";
            case 3:
            return "Đã khám";
            case 4:
            return "Hủy lịch";
            default:
            return "Chờ duyệt";
        }
    }
  }


  public interface TYPE_GET {
      Long ALL = 99L;
    }


    public final class HEADER_FIELD {
    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
  }


}
