package com.huynv.dentcareWebApi.controller.endpoint;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.dto.response.ResponseData;
import com.huynv.dentcareWebApi.entity.AdmMedical;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.exception.BaseException;
import com.huynv.dentcareWebApi.exception.Result;
import com.huynv.dentcareWebApi.service.MedicalService;
import com.huynv.dentcareWebApi.utils.DateUtils;
import com.huynv.dentcareWebApi.utils.JsonHelper;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import net.sf.jett.transform.ExcelTransformer;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/medical", produces = MediaType.APPLICATION_JSON_VALUE)
public class AdmMedicalController {

    @Autowired
    private MedicalService medicalService;
    @Autowired
    private MessageSource messageSource;
    // thêm sửa xoá lấy danh sách lịch hẹn
    //1. thêm lịch hẹn
    @PostMapping(value = "/add")
    public ResponseEntity<ResponseData> addDoctor(@RequestBody AdmMedical admMedical) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(medicalService.save(admMedical), Result.SUCCESS), HttpStatus.OK);
    }
    //2. sửa lịch hẹn
    @PutMapping
    public ResponseEntity<ResponseData> updateDoctor(@RequestBody AdmMedical admMedical) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(medicalService.update(admMedical), Result.SUCCESS), HttpStatus.OK);
    }
    //3. xoá lịch hẹn
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ResponseData> deleteDoctor(@PathVariable Long id) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(medicalService.deleteById(id), Result.SUCCESS), HttpStatus.OK);
    }
    //4. lấy danh sách lịch hẹn
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Danh sách", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/getPage")
    public ResponseEntity<ResponseData> getPage(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
            @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
            @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page<AdmMedical> pages = medicalService.getPaging(searchObject, pageable);
        return new ResponseEntity<>(new ResponseData<>(pages, Result.SUCCESS), HttpStatus.OK);
    }
    //    5. xoá nhiều 1 lúc
    @DeleteMapping(value = "/deleteIds")
    public ResponseEntity<ResponseData> deleteIds(@RequestBody List<Long> ids) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(medicalService.deleteIds(ids), Result.SUCCESS), HttpStatus.OK);
    }
    // 6. Lấy chi tiết theo id
    @GetMapping(value = "/{id}")
    public ResponseEntity<ResponseData> getDetailEquipment(@PathVariable("id") @Valid Long id) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(medicalService.get(id).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"HomePage"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }

    // 7. Lấy all danh sách
    @GetMapping(value = "/findAll")
    public ResponseEntity<ResponseData> getAllEquipment() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(medicalService.getAll(), Result.SUCCESS), HttpStatus.OK);
    }

    //8. Xuất excel
    @GetMapping("/exportExcel")
    public void exportExcelTransPkg(HttpServletResponse response, HttpServletRequest request,
                                    @RequestParam(value = "page", defaultValue = "0") int page,
                                    @RequestParam(value = "size", defaultValue = "10") int size,
                                    @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
                                    @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
                                    @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page<AdmMedical> pages = medicalService.getPaging(searchObject, pageable);
        try {
            String transTemplate = "/fileTemplate/list_medical.xlsx";

            Map<String, Object> beans = new HashMap<>();
            beans.put("page", pages);
            Resource resource = new ClassPathResource(transTemplate);
            InputStream fileIn = resource.getInputStream();
            ExcelTransformer transformer = new ExcelTransformer();
            Workbook workbook = transformer.transform(fileIn, beans);

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + DateUtils.convertDateToStringWithType(new Date(), "ddMMyyyyHHmmss") + "_list_medical.xlsx");
            ServletOutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
