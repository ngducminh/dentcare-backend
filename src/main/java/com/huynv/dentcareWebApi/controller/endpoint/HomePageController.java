package com.huynv.dentcareWebApi.controller.endpoint;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.response.ResponseData;
import com.huynv.dentcareWebApi.entity.AdmCustomer;
import com.huynv.dentcareWebApi.entity.AdmDoctor;
import com.huynv.dentcareWebApi.entity.AdmHomePage;
import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.exception.BaseException;
import com.huynv.dentcareWebApi.exception.Result;
import com.huynv.dentcareWebApi.service.AdmHomePageService;
import com.huynv.dentcareWebApi.service.AdmUserService;
import com.huynv.dentcareWebApi.utils.DateUtils;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import com.huynv.dentcareWebApi.utils.UtilsDate;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/system/homePage", produces = MediaType.APPLICATION_JSON_VALUE)
public class HomePageController {
    //PreAuthorize -> hasAuthority -> dành cho những role đã bắt đầu ROLE_
    //PreAuthorize -> hasRole -> dành cho những role chưa bắt đầu ROLE_            recommended  hasRole('ROLE_SYSTEM_USER')
    //Secured -> là thuộc tính or giữa nhiều ROLE_

    @Autowired
    private AdmHomePageService admHomePageService;
    @Autowired
    private MessageSource messageSource;

    // lấy thông tin trang chủ
    @ApiOperation(response = AdmDoctor.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy thông tin trang chủ", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/{id}")
    public ResponseEntity<ResponseData> getDetailHomePage(@PathVariable("id") @Valid Long id) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admHomePageService.getHomePage(id).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"HomePage"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }

    //edit homePage
    @PutMapping(value = "/edit")
    public ResponseEntity<ResponseData> edit(@RequestBody @Valid AdmHomePage homePage) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admHomePageService.edit(homePage), Result.SUCCESS), HttpStatus.OK);
    }

    //tính doanh thu
    @GetMapping(value = "/tinh-doanh-thu")
    public ResponseEntity<ResponseData> tinhDoanhThu(
            @RequestParam(value = "from", required = false) @Valid Long from,
            @RequestParam(value = "to", required = false) @Valid Long to
    ) throws BadRequestException {
        Date from_ = UtilsDate.convertFromLong(from);
        Date to_ = UtilsDate.convertFromLong(to);
        List<ViewDoanhThu> l = admHomePageService.tinhDoanhThu(from_, to_);
        return new ResponseEntity<>(new ResponseData<>(l, Result.SUCCESS), HttpStatus.OK);
    }

    @GetMapping(value = "/tinh-doanh-thu-theo-dich-vu")
    public ResponseEntity<ResponseData> tinhDoanhThu1(
            @RequestParam(value = "userId", required = false) @Valid Long userId,
            @RequestParam(value = "from", required = false) @Valid Long from,
            @RequestParam(value = "to", required = false) @Valid Long to
    ) throws BadRequestException {
        Date from_ = UtilsDate.convertFromLong(from);
        Date to_ = UtilsDate.convertFromLong(to);
        List<ViewDoanhThu1> l = admHomePageService.tinhDoanhThuTheoDichVu(userId, from_, to_);
        return new ResponseEntity<>(new ResponseData<>(l, Result.SUCCESS), HttpStatus.OK);
    }

    @GetMapping(value = "/thong-ke-theo-trang-thai")
    public ResponseEntity<ResponseData> tinhDoanhThu2(
            @RequestParam(value = "userId", required = false) @Valid Long userId,
            @RequestParam(value = "from", required = false) @Valid Long from,
            @RequestParam(value = "to", required = false) @Valid Long to
    ) throws BadRequestException {
        Date from_ = UtilsDate.convertFromLong(from);
        Date to_ = UtilsDate.convertFromLong(to);
        List<ThongKeTrangThai> l = admHomePageService.thongkeTrangThai(userId, from_, to_);
        return new ResponseEntity<>(new ResponseData<>(l, Result.SUCCESS), HttpStatus.OK);
    }
}
