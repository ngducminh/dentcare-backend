package com.huynv.dentcareWebApi.controller.endpoint;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.AddDoctorDTO;
import com.huynv.dentcareWebApi.dto.request.ChangePass;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.dto.response.ResponseData;
import com.huynv.dentcareWebApi.entity.*;
import com.huynv.dentcareWebApi.entity.view.ViewAdmUser;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.exception.BaseException;
import com.huynv.dentcareWebApi.exception.Result;
import com.huynv.dentcareWebApi.service.AdmUserService;
import com.huynv.dentcareWebApi.utils.DateUtils;
import com.huynv.dentcareWebApi.utils.JsonHelper;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import net.sf.jett.transform.ExcelTransformer;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/system/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    //PreAuthorize -> hasAuthority -> dành cho những role đã bắt đầu ROLE_
    //PreAuthorize -> hasRole -> dành cho những role chưa bắt đầu ROLE_            recommended  hasRole('ROLE_SYSTEM_USER')
    //Secured -> là thuộc tính or giữa nhiều ROLE_

    @Autowired
    private AdmUserService admUserService;
    @Autowired
    private MessageSource messageSource;

    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Danh sách người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/getPage")
    public ResponseEntity<ResponseData> getPage(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
            @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
            @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page pages = admUserService.getPage(searchObject, pageable);
        return new ResponseEntity<>(new ResponseData<>(pages, Result.SUCCESS), HttpStatus.OK);
    }
    @GetMapping("/exportExcel")
    public void exportExcelTransPkg(HttpServletResponse response, HttpServletRequest request,
                                    @RequestParam(value = "page", defaultValue = "0") int page,
                                    @RequestParam(value = "size", defaultValue = "10") int size,
                                    @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
                                    @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
                                    @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page pages = admUserService.getPage(searchObject, pageable);
        try {
            String transTemplate = "/fileTemplate/list_bac_sy.xlsx";
            if(searchObject.getSearchType().equals(1L)) {
                transTemplate = "/fileTemplate/list_nhan_vien.xlsx";
            } else if(searchObject.getSearchType().equals(2L)) {
                transTemplate = "/fileTemplate/list_khach_hang.xlsx";
            }
            else if(searchObject.getSearchType().equals(4L)) {
                transTemplate = "/fileTemplate/list_tai_khoan.xlsx";
            }



            Map<String, Object> beans = new HashMap<>();
            beans.put("page", pages);
            Resource resource = new ClassPathResource(transTemplate);
            InputStream fileIn = resource.getInputStream();
            ExcelTransformer transformer = new ExcelTransformer();
            Workbook workbook = transformer.transform(fileIn, beans);

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + DateUtils.convertDateToStringWithType(new Date(), "ddMMyyyyHHmmss") + "_list_nguoi_dung.xlsx");
            ServletOutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Thêm mới người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping
    public ResponseEntity<ResponseData> add(@RequestBody @Valid AdmUser user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.add(user), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy người dùng theo ID", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/{userId}")
    public ResponseEntity<ResponseData> initEdit(@PathVariable("userId") @Valid Long userId) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.get(userId).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"User"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }

    //addCustomer
    @PostMapping(value = "/addCustomer")
    public ResponseEntity<ResponseData> addCustomer(@RequestBody @Valid AdmCustomer user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.addCustomer(user), Result.SUCCESS), HttpStatus.OK);
    }

    //addDoctor
    @PostMapping(value = "/addDoctor")
    public ResponseEntity<ResponseData> addDoctor(@RequestBody AddDoctorDTO dto) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.addDoctor(dto.getUser(), dto.getSpecializes(), dto.getEx()), Result.SUCCESS), HttpStatus.OK);
    }

    //addStaff
    @PostMapping(value = "/addStaff")
    public ResponseEntity<ResponseData> addStaff(@RequestBody @Valid AdmStaff user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.addStaff(user), Result.SUCCESS), HttpStatus.OK);
    }

    //editCustomer
    @PutMapping(value = "/editCustomer")
    public ResponseEntity<ResponseData> editCustomer(@RequestBody @Valid AdmCustomer user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.editCustomer(user), Result.SUCCESS), HttpStatus.OK);
    }
    //editDoctor
    @PutMapping(value = "/editDoctor")
    public ResponseEntity<ResponseData> editDoctor(@RequestBody AddDoctorDTO dto) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.editDoctor(dto.getUser(), dto.getSpecializes(), dto.getEx()), Result.SUCCESS), HttpStatus.OK);
    }
    //edit Staff
    @PutMapping(value = "/editStaff")
    public ResponseEntity<ResponseData> editStaff(@RequestBody @Valid AdmStaff user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.editStaff(user), Result.SUCCESS), HttpStatus.OK);
    }
    //deleteCustomer
    @DeleteMapping(value = "/deleteCustomer")
    public ResponseEntity<ResponseData> deleteCustomer(@RequestBody List<Long> ids) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.deleteCustomer(ids), Result.SUCCESS), HttpStatus.OK);
    }
    //deleteDoctor
    @DeleteMapping(value = "/deleteDoctor")
    public ResponseEntity<ResponseData> deleteDoctor(@RequestBody List<Long> ids) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.deleteDoctor(ids), Result.SUCCESS), HttpStatus.OK);
    }
    //deleteStaff
    @DeleteMapping(value = "/deleteStaff")
    public ResponseEntity<ResponseData> deleteStaff(@RequestBody List<Long> ids) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.deleteStaff(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmDoctor.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy thông tin bác sĩ theo ID", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/doctor/{doctorId}")
    public ResponseEntity<ResponseData> getDetailDoctor(@PathVariable("doctorId") @Valid Long doctorId) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getDoctor(doctorId).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"Doctor"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmStaff.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy thông tin nhân viên theo ID", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/staff/{staffId}")
    public ResponseEntity<ResponseData> getDetailStaff(@PathVariable("staffId") @Valid Long staffId) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getStaff(staffId).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"Staff"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmCustomer.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy thông tin khách hàng theo ID", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/customer/{customerId}")
    public ResponseEntity<ResponseData> getDetailCustomer(@PathVariable("customerId") @Valid Long customerId) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getCustomer(customerId).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"Customer"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }


    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Chỉnh sửa người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PutMapping
    public ResponseEntity<ResponseData> edit(@RequestBody AdmUser dto) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.edit(dto), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmUser[].class, notes = Constants.NOTE_API + "empty_note", value = "Xóa người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @DeleteMapping(path = "/delete")
    public ResponseEntity<ResponseData> deleteByIds(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.deleteByIds(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmUser[].class, notes = Constants.NOTE_API + "empty_note", value = "Khóa người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/lock")
    public ResponseEntity<ResponseData> locks(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.locks(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmUser[].class, notes = Constants.NOTE_API + "empty_note", value = "Mở khóa người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/unlock")
    public ResponseEntity<ResponseData> unlocks(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.unlocks(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmDoctor[].class, notes = Constants.NOTE_API + "empty_note", value = "Khóa bác sĩ", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/doctor/lock")
    public ResponseEntity<ResponseData> lockDoctors(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.lockDoctors(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmDoctor[].class, notes = Constants.NOTE_API + "empty_note", value = "Mở khóa bác sĩ", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/doctor/unlock")
    public ResponseEntity<ResponseData> unlockDoctors(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.unlockDoctors(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmStaff[].class, notes = Constants.NOTE_API + "empty_note", value = "Khóa nhân viên", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/staff/lock")
    public ResponseEntity<ResponseData> lockStaffs(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.lockStaffs(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmStaff[].class, notes = Constants.NOTE_API + "empty_note", value = "Mở khóa nhân viên", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/staff/unlock")
    public ResponseEntity<ResponseData> unlockStaffs(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.unlockStaffs(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmCustomer[].class, notes = Constants.NOTE_API + "empty_note", value = "Khóa khách hàng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/customer/lock")
    public ResponseEntity<ResponseData> lockCustomers(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.lockCustomers(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @ApiOperation(response = AdmCustomer[].class, notes = Constants.NOTE_API + "empty_note", value = "Mở khóa khách hàng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(path = "/customer/unlock")
    public ResponseEntity<ResponseData> unlockCustomers(@RequestBody @Valid List<Long> ids) {
        return new ResponseEntity<>(new ResponseData<>(admUserService.unlockCustomers(ids), Result.SUCCESS), HttpStatus.OK);
    }

    @GetMapping(value = "/findAll")
    public ResponseEntity<ResponseData> getAll() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getAll(), Result.SUCCESS), HttpStatus.OK);
    }

    @GetMapping(value = "/doctor/findAll")
    public ResponseEntity<ResponseData> getAllDoctor() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getAllDoctor(), Result.SUCCESS), HttpStatus.OK);
    }

    @GetMapping(value = "/customer/findAll")
    public ResponseEntity<ResponseData> getAllCustomer() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getAllCustomer(), Result.SUCCESS), HttpStatus.OK);
    }

    @GetMapping(value = "/staff/findAll")
    public ResponseEntity<ResponseData> getAllStaff() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.getAllStaff(), Result.SUCCESS), HttpStatus.OK);
    }

    //get info me
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy thông tin người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/me")
    public ResponseEntity<ResponseData> getMe() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(UtilsCommon.getUserLogin(), Result.SUCCESS), HttpStatus.OK);
    }

    //đổi mật khẩu
    @PostMapping(value = "/change-password")
    public ResponseEntity<ResponseData> changePassword(@RequestBody @Valid ChangePass dto) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.changePassword(dto), Result.SUCCESS), HttpStatus.OK);
    }

}
