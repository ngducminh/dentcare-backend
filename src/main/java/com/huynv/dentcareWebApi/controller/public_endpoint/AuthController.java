package com.huynv.dentcareWebApi.controller.public_endpoint;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.ChangePass;
import com.huynv.dentcareWebApi.dto.request.LoginRequest;
import com.huynv.dentcareWebApi.dto.request.ResetPass;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.dto.response.ResponseData;
import com.huynv.dentcareWebApi.entity.*;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.exception.BaseException;
import com.huynv.dentcareWebApi.exception.Result;
import com.huynv.dentcareWebApi.exception.UnauthorizedException;
import com.huynv.dentcareWebApi.service.*;
import com.huynv.dentcareWebApi.utils.JsonHelper;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Slf4j
@RestController
@RequestMapping(value = "/public/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    @Autowired private MessageSource messageSource;
    @Autowired private SrsSystemSercice srsSystemSercice;
    @Autowired private AdmHomePageService admHomePageService;
    @Autowired private AdmServiceService admServiceService;
    @Autowired private AdmUserService admUserService;
    @Autowired private AppointmentService appointmentService;
    @Autowired private MedicalService medicalService;
    @Autowired private EmailService emailService;

    // login
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Login đăng nhập", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(value = "/jwt-login")
    public ResponseEntity<ResponseData> loginJWT(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) throws UnauthorizedException, IOException, InvalidKeySpecException, NoSuchAlgorithmException, UnauthorizedException, BadRequestException {
        System.out.println(request.getHeader("Accept-Language"));
        log.info("language current: " + UtilsCommon.getLocale() + ", examle error_401: " + messageSource.getMessage("error_401", null, UtilsCommon.getLocale()));
        loginRequest.setIp(request.getRemoteAddr());
        loginRequest.setUserAgent(request.getHeader("User-Agent"));
        return new ResponseEntity<>(new ResponseData<>(srsSystemSercice.loginJWT(loginRequest), Result.SUCCESS), HttpStatus.OK);
    }

    // change pass from web
    @PostMapping(value = "/change-password")
    public ResponseEntity<ResponseData> changePassword(@RequestBody @Valid ChangePass dto) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.changePassword(dto), Result.SUCCESS), HttpStatus.OK);
    }

    // add lịch hẹn from web
    @PostMapping(value = "/add")
    public ResponseEntity<ResponseData> addDoctor(@RequestBody AdmAppointment admAppointment) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(appointmentService.save(admAppointment), Result.SUCCESS), HttpStatus.OK);
    }

    //add ser from webite
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Thêm mới người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping("/register")
    public ResponseEntity<ResponseData> add(@RequestBody @Valid AdmUser user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.add(user), Result.SUCCESS), HttpStatus.OK);
    }

    @PostMapping(value = "/addCustomer")
    public ResponseEntity<ResponseData> addCustomer(@RequestBody @Valid AdmCustomer user) throws BadRequestException {
        return new ResponseEntity<>(new ResponseData<>(admUserService.addCustomer(user), Result.SUCCESS), HttpStatus.OK);
    }

    //lấy thông tin trang client
    @ApiOperation(response = AdmHomePage.class, notes = Constants.NOTE_API + "empty_note", value = "Lấy thông tin trang chủ", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/getHomeInfo/{id}")
    public ResponseEntity<ResponseData> getDetailHomePage(@PathVariable("id") @Valid Long id) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admHomePageService.getHomePage(id).orElseThrow(() -> new BaseException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"HomePage"}, UtilsCommon.getLocale()))), Result.SUCCESS), HttpStatus.OK);
    }

    //Lấy danh sách dịch vụ
    @GetMapping(value = "/findAllService")
    public ResponseEntity<ResponseData> getAllEquipment() throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admServiceService.getAll(), Result.SUCCESS), HttpStatus.OK);
    }

    //lấy danh sách bác sĩ
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Danh sách người dùng", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/getPageDoctor")
    public ResponseEntity<ResponseData> getPageDoctor(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
            @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
            @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page pages = admUserService.getPage(searchObject, pageable);
        return new ResponseEntity<>(new ResponseData<>(pages, Result.SUCCESS), HttpStatus.OK);
    }

    //4. lấy danh sách lịch hẹn
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Danh sách", authorizations = {@Authorization(value = Constants.API_KEY)})
        @GetMapping(value = "/getPageAppointment")
    public ResponseEntity<ResponseData> getPageAppointment(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
            @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
            @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page<AdmAppointment> pages = appointmentService.getPaging(searchObject, pageable);
        return new ResponseEntity<>(new ResponseData<>(pages, Result.SUCCESS), HttpStatus.OK);
    }
    //5. lấy danh sách phiếu khám
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Danh sách", authorizations = {@Authorization(value = Constants.API_KEY)})
    @GetMapping(value = "/getPage")
    public ResponseEntity<ResponseData> getPage(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sortBy", defaultValue = "modifiedDate") String sortBy,
            @RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
            @ApiParam(value = Constants.NOTE_API_PAGEABLE) @RequestParam(value = "search") String search) {
        SearchForm searchObject = JsonHelper.jsonToObject(search == null || search.isEmpty() ? "{}" : search, SearchForm.class);
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortType), sortBy));
        Page<AdmMedical> pages = medicalService.getPaging(searchObject, pageable);
        return new ResponseEntity<>(new ResponseData<>(pages, Result.SUCCESS), HttpStatus.OK);
    }

    //6. reset password
    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Reset password", authorizations = {@Authorization(value = Constants.API_KEY)})
    @PostMapping(value = "/reset-password")
    public ResponseEntity<ResponseData> restPassword(@RequestBody @Valid ResetPass form) throws Throwable {
        return new ResponseEntity<>(new ResponseData<>(admUserService.resetPassword(form), Result.SUCCESS), HttpStatus.OK);
    }


//    @ApiOperation(response = AdmUser.class, notes = Constants.NOTE_API + "empty_note", value = "Reset password", authorizations = {@Authorization(value = Constants.API_KEY)})
//    @PostMapping(value = "/adm-reset-password")
//    public ResponseEntity<ResponseData> resetPassword(@RequestBody @Valid String username) throws Throwable {
//        return new ResponseEntity<>(new ResponseData<>(admUserService.admResetPassword(username), Result.SUCCESS), HttpStatus.OK);
//    }

    // send mail
    @PostMapping("/sendMail")
    public String
    sendMail(@RequestBody EmailDetails details)
    {
        String status = emailService.sendSimpleMail(details);
        return status;
    }

    // Sending email with attachment
    @PostMapping("/sendMailWithAttachment")
    public String sendMailWithAttachment(
            @RequestBody EmailDetails details)
    {
        String status
                = emailService.sendMailWithAttachment(details);

        return status;
    }
}
