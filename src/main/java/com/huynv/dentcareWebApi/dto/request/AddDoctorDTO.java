package com.huynv.dentcareWebApi.dto.request;

import com.huynv.dentcareWebApi.entity.AdmDoctor;
import com.huynv.dentcareWebApi.entity.AdmSpecialize;
import lombok.Data;

import java.util.List;

@Data
public class AddDoctorDTO {
    AdmDoctor user;
    List<AdmSpecialize> specializes;
    List<Integer> ex;
}

