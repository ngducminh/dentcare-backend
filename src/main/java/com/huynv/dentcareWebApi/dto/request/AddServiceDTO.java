package com.huynv.dentcareWebApi.dto.request;

import com.huynv.dentcareWebApi.entity.AdmEquipment;
import com.huynv.dentcareWebApi.entity.AdmService;
import lombok.Data;

import java.util.List;

@Data
public class AddServiceDTO {
    AdmService service;
    List<AdmEquipment> equipments;
    List<Integer> slg;
}
