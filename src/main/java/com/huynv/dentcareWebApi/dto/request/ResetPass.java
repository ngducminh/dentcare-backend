package com.huynv.dentcareWebApi.dto.request;

import lombok.Data;

@Data
public class ResetPass {
    private String username;
    private String email;
    private String phoneNumber;
    private Long type;
}
