package com.huynv.dentcareWebApi.dto.request;

import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.UtilsDate;
import lombok.*;

import java.util.Date;
import java.util.List;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchForm {
    private boolean init = false;
    private List<String> createBys;


    // AdmUser
    private String userId;
    private String fullName;
    private List<Long> groupIds;
    private List<String> authorities;
    private String email;
    private String address;
    private String phone;
    private String description;
    private Long status;
    private String isDelete;
    private String username;
    private String phoneNumber;
    private String typeRegistration;

    private Long price;

    private  String permission;
    private Long typeGet;
    private  String doctorCode;
    private  Long doctorId;
    private  Long customerId;
    private  String staffCode;
    private  String customerCode;
    private String appointmentCode;
    //medicalCode
    private String medicalCode;
    //diagnostic
    private String diagnostic;
    //prescription
    private String prescription;
    private Date bookingDateFrom;
    private Date bookingDateTo;
    private Long bookingDateFromLong;
    private Long bookingDateToLong;
    private Date medicalDateFrom;
    private Date medicalDateTo;
    private Long medicalDateFromLong;
    private Long medicalDateToLong;

    // AdmParaSystem
    private String name;
    private String value;

    // AdmRight
    private String parentId;
    private String rightCode;
    private String rightName;
    private String rightOrder;
    private String urlRewrite;
    private String iconUrl;
    private String badgeSql;

    // AdmAuthorities
    private String keyword;
    private String authKey;
    private String authoritieName;
    private String groupName;
    private Date fromDate;
    private Date toDate;

    // wso2is
    private String attributes;
    private String filter;
    private String startIndex;
    private String count;
    private String domain;

    // SchedulerJobInfo
    private Long type;
    private String jobName;
    private String jobGroup;
    private String jobClass;
    private String jobFullName;
    private String cronExpression;
    private String repeatTime;
    private Boolean cronJob;
    private String typeSyn;
    private String month;
    private String year;
    private String day;
    private String hour;
    private String minute;
    private String weekDay;
    private String weekTime;

    // AdmDept
    private String deptName;
    private String deptDesc;

    // CmCommune
    private String communeName;
    private String communeCode;

    //CmDistrict
    private String districtName;
    private String districtCode;

    // CmProvince
    private String provinceCode;
    private String provinceName;

    private Long searchType;



    // Data Info Init
    private String excelName;
    private Integer rowNumber;
    private String provinceIdStr;
    private String districtIdStr;
    private String communeIdStr;
    private String fromCreateDate;
    private String toCreateDate;
    private String fromUpdateDate;
    private String toUpdateDate;
    private List<String> updateBys;

    private Long fromDateLong;
    private Long toDateLong;

    private String modifiedBy;

    private Date modifiedDateFrom;
    private Date modifiedDateTo;

    private Long modifiedDateFromLong;
    private Long modifiedDateToLong;

    private String plateNumber;

    private Long id;
    private Long serviceId;
    private Long specializeId;
    private Long equipmentId;
    private List<Long> equipmentIds;

    public Date getModifiedDateFrom() {
//        return modifiedDateFrom;
        if (H.isTrue(this.modifiedDateFrom)) {
            return this.modifiedDateFrom;
        } else if (H.isTrue(this.modifiedDateFromLong)) {
            return UtilsDate.convertFromLong(this.modifiedDateFromLong);
        } else {
            return null;
        }
    }

    public Date getModifiedDateTo() {
//        return modifiedDateTo;
        if (H.isTrue(this.modifiedDateTo)) {
            return this.modifiedDateTo;
        } else if (H.isTrue(this.modifiedDateToLong)) {
            return UtilsDate.convertFromLong(this.modifiedDateToLong);
        } else {
            return null;
        }
    }

    public String getFromDateStr() {
        if (H.isTrue(fromDateLong)) {
            return UtilsDate.date2str(UtilsDate.convertFromLong(fromDateLong), "dd/MM/yyyy");
        } else if (H.isTrue(fromDate)) {
            return UtilsDate.date2str(fromDate, "dd/MM/yyyy");
        }
        return null;
    }

    public String getToDateStr() {
        if (H.isTrue(toDateLong)) {
            return UtilsDate.date2str(UtilsDate.convertFromLong(toDateLong), "dd/MM/yyyy");
        } else if (H.isTrue(toDate)) {
            return UtilsDate.date2str(toDate, "dd/MM/yyyy");
        }
        return null;
    }

    public Date getBookingDateFrom() {
        if(H.isTrue(this.bookingDateFrom)){
            return this.bookingDateFrom;
        }else if(H.isTrue(this.bookingDateFromLong)){
            return UtilsDate.convertFromLong(this.bookingDateFromLong);
        }
        return null;
    }

    public Date getBookingDateTo() {
        if(H.isTrue(this.bookingDateTo)){
            return this.bookingDateTo;
        }else if(H.isTrue(this.bookingDateToLong)){
            return UtilsDate.convertFromLong(this.bookingDateToLong);
        }
        return null;
    }

    public Date getMedicalDateFrom() {
        if(H.isTrue(this.medicalDateFrom)){
            return this.medicalDateFrom;
        }else if(H.isTrue(this.medicalDateFromLong)){
            return UtilsDate.convertFromLong(this.medicalDateFromLong);
        }
        return null;
    }

    public Date getMedicalDateTo() {
        if(H.isTrue(this.medicalDateTo)){
            return this.medicalDateTo;
        }else if(H.isTrue(this.medicalDateToLong)){
            return UtilsDate.convertFromLong(this.medicalDateToLong);
        }
        return null;
    }
}
