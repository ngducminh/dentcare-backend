package com.huynv.dentcareWebApi.dto.request;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

    private String username;
    private String mobileAlias;
    private String email;
    private ISTokenInfo accessTokenInfo;
    private List<String> authorities;

}
