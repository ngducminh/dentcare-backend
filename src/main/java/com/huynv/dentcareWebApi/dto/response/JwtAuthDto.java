package com.huynv.dentcareWebApi.dto.response;

import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author sangnk
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtAuthDto {

  private String issuer;
  private String ui;
  private String uname;
  private List roles;
  private String email;
  private Date expi;
  private String usAgent;
  private String ipAddress;
  private String ss;
  private String fullname;

}
