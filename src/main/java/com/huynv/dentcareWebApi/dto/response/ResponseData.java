package com.huynv.dentcareWebApi.dto.response;

import com.huynv.dentcareWebApi.exception.Result;
import lombok.*;

import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {

    private int code;
    private String message;
    private T data;                     // truyền common (input anything string, number)
//    private T typeOfBody;
    private String path;
    private Date time;

    public ResponseData(Result result) {
        this.code = result.getCode();
        this.message = result.getMessage();
    }

    public ResponseData(T data, Result result) {
        this.data = data;
        this.code = result.getCode();
        this.message = result.getMessage();
    }

    public ResponseData (T data, Result result, String message){
        this.data = data;
        this.code = result.getCode();
        this.message = message;
    }

}

