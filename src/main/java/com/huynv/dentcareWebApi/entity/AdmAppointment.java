package com.huynv.dentcareWebApi.entity;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.utils.DateUtils;
import com.huynv.dentcareWebApi.utils.H;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

// lombok annotations thư viện java tự động tạo các getter, ....
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_APPOINTMENT")
public class AdmAppointment extends Auditable{
    // appointmentCode
    @Size(max = 64)
    @NotNull
    @Column(name = "APPOINTMENTCODE", nullable = false, length = 64) // các cột column show độ dài tối đa (length) hoặc giá trị mặc định
    private String appointmentCode;

    // Status
    @Column(name = "STATUS", columnDefinition = "bigint default 0")
    private Long status= Constants.APPOINTMENT_TYPE.CHO_DUYET;

    // note
    @Size(max = 2000)
    @Column(name = "NOTE", length = 2000)
    private String note;

    //bookingDate
    @Column(name = "BOOKINGDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bookingDate;

    @ManyToOne                              //quan he với các bảng
    @JoinColumn(name = "customer_id")
    private AdmCustomer admCustomer;

    @ManyToOne
    @JoinColumn(name = "staff_id")
    private AdmStaff admStaff;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private AdmDoctor admDoctor;

    //many to many with service 1 bảng có thể appointment có thể liên kết với nhiều service và ngược lại
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ADM_APPOINTMENT_SERVICE",
            joinColumns = @JoinColumn(name = "APPOINTMENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "SERVICE_ID"))
    private List<AdmService> services;

    @Transient                              // thuộc tính tạm thời, không được lưu trữ trong db
    private String bookingDateStr;

    @Transient                  // thuộc tính tạm thời, không được lưu trữ trong db
    private String statusStr;

    public String getStatusStr() {                                  //trả về chuỗi
        if(H.isTrue((this.status))) return Constants.APPOINTMENT_TYPE.getAppointmentType(status); //check nếu đúng thì xử lý quyền
        return null;
    }

    public String getBookingDateStr() {
        if(H.isTrue((this.bookingDate))) return DateUtils.convertDateToStringWithType(bookingDate, "dd/MM/yyyy, HH:mm");
        return null;
    }

    public AdmAppointment formToBo(AdmAppointment form, AdmAppointment bo) {  //sao chép dữ diệu từ form sang bo, gán kèm các childs
        bo.setAppointmentCode(form.getAppointmentCode());
        bo.setStatus(form.getStatus());
        bo.setBookingDate(form.getBookingDate());
        bo.setNote(form.getNote());
        bo.setAdmCustomer(form.getAdmCustomer());
        bo.setAdmDoctor(form.getAdmDoctor());
        bo.setAdmStaff(form.getAdmStaff());
        if(H.isTrue(form.getServices())){       // kiểm tra service null
            bo.setServices(form.getServices());
        }
        return bo;
    }               // dùng để cập nhập đã tồn tại
}
