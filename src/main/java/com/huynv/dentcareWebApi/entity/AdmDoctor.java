package com.huynv.dentcareWebApi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.utils.DateUtils;
import com.huynv.dentcareWebApi.utils.H;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_DOCTOR")
public class AdmDoctor extends Auditable{
    // doctorCode
    @Size(max = 64)
    @NotNull
    @Column(name = "DOCTORCODE", nullable = false, length = 64)
    private String doctorCode;

    //fullName
    @Column(name = "FULLNAME")
    private String fullName;

    //gender
    @Column(name = "GENDER", columnDefinition = "bigint default 0")
    private Long gender=Constants.GENDER.MALE;

    //email
    @Column(name = "EMAIL")
    private String email;

    //address
    @Column(name = "ADDRESS")
    private String address;

    //dob
    @Column(name = "DOB")
    private Date dob;

    //phoneNumber
    @Column(name = "PHONENUMBER")
    private String phoneNumber;

    // Status
    @Column(name = "STATUS", columnDefinition = "bigint default 0")
    private Long status= Constants.STATUS.ACTIVE;

    // note
    @Size(max = 2000)
    @Column(name = "NOTE", length = 2000)
    private String note;

    //image - base64
    @Column(name = "IMAGE", columnDefinition = "longtext")
    private String image;

    @OneToOne
    @JoinColumn(name = "adm_user_id", referencedColumnName = "ID")
    private AdmUser admUser;

    // many to many with service
    @JsonIgnore
    @ManyToMany(mappedBy = "doctors", fetch = FetchType.LAZY)
    private List<AdmService> services;

    @Transient
    private String dobStr;

    @OneToMany(mappedBy = "admDoctor", fetch = FetchType.LAZY)
    private List<AdmDoctorSpecialize> admDoctorSpecializes;

    public String getDobStr() {
        if(H.isTrue(dob)) {return DateUtils.convertDateToStringWithType(dob, "dd/MM/yyyy");}
        return null;
    }

    public AdmDoctor formToBo(AdmDoctor form, AdmDoctor bo) {
        bo.setDoctorCode(form.getDoctorCode());
        bo.setFullName(form.getFullName());
        bo.setGender(form.getGender());
        bo.setEmail(form.getEmail());
        bo.setAddress(form.getAddress());
        bo.setDob(form.getDob());
        bo.setPhoneNumber(form.getPhoneNumber());
        bo.setStatus(form.getStatus());
        bo.setNote(form.getNote());
        bo.setImage(form.getImage());
//        bo.setAdmUser(from.getAdmUser());
        if(H.isTrue(form.getServices())){
            bo.setServices(form.getServices());
        }
        return bo;
    }


}
