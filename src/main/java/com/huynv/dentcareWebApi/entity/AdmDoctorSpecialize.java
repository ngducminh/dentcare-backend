package com.huynv.dentcareWebApi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_DOCTOR_SPECIALIZE")
public class AdmDoctorSpecialize extends Auditable{
    //experience
    @Column(name = "EXPERIENCE")
    private Integer experience;

    @ManyToOne
    @JoinColumn(name = "specialize_id")
    private AdmSpecialize admSpecialize;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "doctor_id")
    private AdmDoctor admDoctor;
}
