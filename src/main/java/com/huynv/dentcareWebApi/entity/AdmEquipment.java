package com.huynv.dentcareWebApi.entity;

import com.huynv.dentcareWebApi.contants.Constants;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_EQUIPMENT")
public class AdmEquipment extends Auditable {
    // Name
    @Size(max = 256)
    @NotNull
    @Column(name = "NAME", nullable = false, length = 256)
    private String name;

    // Status
    @Column(name = "STATUS", columnDefinition = "bigint default 0")
    private Long status= Constants.STATUS.ACTIVE;

    // Type
    @Column(name = "TYPE", columnDefinition = "bigint default 0")
    private Long type= Constants.EQUIPMENT_TYPE.THIET_BI_CO_BAN;

    //price
    @Column(name = "PRICE")
    private Long price;

    // description
    @Size(max = 2000)
    @Column(name = "DESCRIPTION", length = 2000)
    private String description;

    public AdmEquipment formToBo(AdmEquipment form, AdmEquipment bo) {
        bo.setName(form.getName());
        bo.setStatus(form.getStatus());
        bo.setType(form.getType());
        bo.setPrice(form.getPrice());
        bo.setDescription(form.getDescription());
        return bo;
    }
}
