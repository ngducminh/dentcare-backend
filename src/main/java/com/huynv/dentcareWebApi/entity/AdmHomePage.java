package com.huynv.dentcareWebApi.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_HOME_PAGE")
public class AdmHomePage extends Auditable {

    //address
    @Column(name = "ADDRESS")
    private String address;

    //websiteAddress
    @Column(name = "WEBSITE_ADDRESS")
    private String websiteAddress;

    //email
    @Column(name = "EMAIL")
    private String email;

    //phoneNumber
    @Size(max = 128)
    @NotNull
    @Column(name = "PHONE_NUMBER", nullable = false ,length = 128)
    private String phoneNumber;

    //banner - base64
    @Column(name = "BANNER", columnDefinition = "longtext")
    private String banner;

    public AdmHomePage formToBo(AdmHomePage form, AdmHomePage bo) {
        bo.setAddress(form.getAddress());
        bo.setWebsiteAddress(form.getWebsiteAddress());
        bo.setEmail(form.getEmail());
        bo.setPhoneNumber(form.getPhoneNumber());
        bo.setBanner(form.getBanner());
        return bo;
    }
}
