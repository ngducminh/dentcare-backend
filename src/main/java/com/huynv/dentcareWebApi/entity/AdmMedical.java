package com.huynv.dentcareWebApi.entity;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.utils.DateUtils;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_MEDICAL")
public class AdmMedical extends Auditable{
    // medicalCode
    @Size(max = 64)
    @NotNull
    @Column(name = "MEDICALCODE", nullable = false, length = 64)
    private String medicalCode;

    //medicalDate
    @Column(name = "MEDICALDATE")
    private Date medicalDate;

    // note
    @Size(max = 2000)
    @Column(name = "NOTE", length = 2000)
    private String note;

    // diagnostic : Chẩn đoán
    @Column(name = "DIAGNOSTIC")
    private String diagnostic;

    // prescription : Đơn thuốc
    @Column(name = "PRESCRIPTION")
    private String prescription;

    @ManyToOne
    @JoinColumn(name = "appointment_id")
    private AdmAppointment admAppointment;

    @Transient
    private String medicalDateStr;

    public String getMedicalDateStr() {
        return DateUtils.convertDateToStringWithType(medicalDate, "dd/MM/yyyy, HH:mm");
    }

    public AdmMedical formToBo(AdmMedical form, AdmMedical bo) {
        bo.setMedicalCode(form.getMedicalCode());
        bo.setNote(form.getNote());
        bo.setDiagnostic(form.getDiagnostic());
        bo.setPrescription(form.getPrescription());
        bo.setAdmAppointment(form.getAdmAppointment());
        return bo;
    }
}
