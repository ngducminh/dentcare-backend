package com.huynv.dentcareWebApi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.huynv.dentcareWebApi.contants.Constants;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_SERVICE")
public class AdmService extends Auditable {
    // Name
    @Size(max = 256)
    @NotNull
    @Column(name = "NAME", nullable = false, length = 256)
    private String name;

    // Status
    @Column(name = "STATUS", columnDefinition = "bigint default 0")
    private Long status= Constants.STATUS.ACTIVE;

    // Type
    @Column(name = "TYPE", columnDefinition = "bigint default 0")
    private Long type= Constants.SERVICE_TYPE.TU_VAN;

    //image - base64
    @Column(name = "IMAGE", columnDefinition = "longtext")
    private String image;

    //price
    @Column(name = "PRICE")
    private Long price;

    // description
    @Size(max = 2000)
    @Column(name = "DESCRIPTION", length = 2000)
    private String description;

    //many to many with Doctor
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ADM_DOCTOR_SERVICE",
            joinColumns = @JoinColumn(name = "SERVICE_ID"),
            inverseJoinColumns = @JoinColumn(name = "DOCTOR_ID"))
    private List<AdmDoctor> doctors;

    // many to many with appointment
    @JsonIgnore
    @ManyToMany(mappedBy = "services", fetch = FetchType.LAZY)
    private List<AdmAppointment> appointments;

    @OneToMany(mappedBy = "admService", fetch = FetchType.LAZY)
    private List<AdmServiceEquipment> admServiceEquipments;

    public List<AdmServiceEquipment> getAdmServiceEquipments() {
        return admServiceEquipments;
    }

    public AdmService formToBo(AdmService form, AdmService bo) {
        bo.setName(form.getName());
        bo.setStatus(form.getStatus());
        bo.setType(form.getType());
        bo.setImage(form.getImage());
        bo.setPrice(form.getPrice());
        bo.setDescription(form.getDescription());
        bo.setDoctors(form.getDoctors());
        return bo;
    }
}
