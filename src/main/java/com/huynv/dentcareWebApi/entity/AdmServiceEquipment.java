package com.huynv.dentcareWebApi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_SERVICE_EQUIPMENT")
public class AdmServiceEquipment extends Auditable {
    //quantity
    @Column(name = "QUANTITY")
    private Integer quantity;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "service_id")
    private AdmService admService;

    @ManyToOne
    @JoinColumn(name = "equipment_id")
    private AdmEquipment admEquipment;
}
