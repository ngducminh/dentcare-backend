package com.huynv.dentcareWebApi.entity;

import com.huynv.dentcareWebApi.contants.Constants;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_SPECIALIZE")
public class AdmSpecialize extends  Auditable{
    // Name
    @Size(max = 256)
    @NotNull
    @Column(name = "NAME", nullable = false, length = 256)
    private String name;

    // Status
    @Column(name = "STATUS", columnDefinition = "bigint default 0")
    private Long status= Constants.STATUS.ACTIVE;

    // description
    @Size(max = 2000)
    @Column(name = "DESCRIPTION", length = 2000)
    private String description;

    public AdmSpecialize formToBo(AdmSpecialize form, AdmSpecialize bo) {
        bo.setName(form.getName());
        bo.setStatus(form.getStatus());
        bo.setDescription(form.getDescription());
        return bo;
    }
}
