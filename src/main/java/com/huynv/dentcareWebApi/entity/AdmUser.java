package com.huynv.dentcareWebApi.entity;

import com.huynv.dentcareWebApi.contants.Constants;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADM_USER")
public class AdmUser extends Auditable implements UserDetails {

    @Size(max = 128)
    @NotNull
    @Column(name = "userame", nullable = false, length = 128, unique = true)
    private String username;

    @Column(name = "password", length = 128, nullable = false)
    private String password;

    @Column(name = "permission", columnDefinition = "bigint default 0")
    private Long permission= Constants.PERMISSION_TYPE.ADMIN;

    //image - base64
    @Column(name = "avatar", columnDefinition = "longtext")
    private String avatar;

    @Column(name = "status", columnDefinition = "bigint default 0")
    private Long status=Constants.STATUS.ACTIVE;

    @Transient
    private List<GrantedAuthority> grantedAuths;

    @Override                   //lớp con thừa kế lớp tra thực hiện triển khai ipml
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuths;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }           //tài khoản k hết hạn

    @Override
    public boolean isAccountNonLocked() {               //check lock or not
        return this.status != null && this.status.compareTo(Constants.STATUS.ACTIVE) == 0;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }           //tài khoản k hết hạn

    @Override
    public boolean isEnabled() {
        return this.status != null && this.status.compareTo(Constants.STATUS.ACTIVE) == 0;
    }
        // check tài khoản có bật hay không
    public AdmUser formToBo(AdmUser form, AdmUser bo) {
        bo.setUsername(form.getUsername());
        bo.setPermission(form.getPermission());
        bo.setAvatar(form.getAvatar());
        bo.setStatus(form.getStatus());
        return bo;

    }
}
