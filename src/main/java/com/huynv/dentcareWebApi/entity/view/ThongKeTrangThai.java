package com.huynv.dentcareWebApi.entity.view;

import lombok.Data;
import org.hibernate.annotations.Subselect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Subselect(value = "select 1")
@Entity
@Data
public class ThongKeTrangThai implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name = "thoi_gian")
    private String thoi_gian;

    @Column(name = "status")
    private String status;

    //loai dich vu
    @Column(name = "tong_lich_hen")
    private String tong_lich_hen;
}
