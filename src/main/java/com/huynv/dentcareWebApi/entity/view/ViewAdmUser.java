
package com.huynv.dentcareWebApi.entity.view;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.huynv.dentcareWebApi.entity.AdmGroup;
import com.huynv.dentcareWebApi.entity.Auditable;
import lombok.*;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.util.List;

/*
* Tạo view bằng @Subselect
* */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Subselect("select * from ADM_USER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ViewAdmUser extends Auditable{
    @Column(name = "username")
    private String username;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "permission")
    private String permission;

    @Column(name = "status")
    private Long status;

}

