package com.huynv.dentcareWebApi.entity.view;

import lombok.Data;
import org.hibernate.annotations.Subselect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Subselect(value = "select 1")
@Entity
@Data
public class ViewDoanhThu implements Serializable {
    @Id
    @Column(name = "thoi_gian")
    private String thoi_gian;

    @Column(name = "doanh_thu")
    private Long doanh_thu;
}
