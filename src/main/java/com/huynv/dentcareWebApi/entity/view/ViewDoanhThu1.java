package com.huynv.dentcareWebApi.entity.view;

import lombok.Data;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.io.Serializable;

@Subselect(value = "select 1")
@Entity
@Data
public class ViewDoanhThu1 implements Serializable {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name = "thoi_gian")
    private String thoi_gian;

    @Column(name = "doanh_thu")
    private Long doanh_thu;

    //loai dich vu
    @Column(name = "loai_dich_vu")
    private Long loai_dich_vu;

    //ten dich vu
    @Column(name = "ten_dich_vu")
    private String ten_dich_vu;
}
