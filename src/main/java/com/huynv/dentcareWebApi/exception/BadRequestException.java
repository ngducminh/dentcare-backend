/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.huynv.dentcareWebApi.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sangnk
 */
@Getter
@Setter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends Exception  {
    //    private String description;
//
//    private static final long serialVersionUID = -6502596312985405760L;
//
//    public BadRequestException(String message) {
//        super(message);
//        this.description = message;
//    }
//
//    public String getDescription() {
//        return description;
//    }
    private HttpStatus status = null;

    private String data = null;

    public BadRequestException() {
        super();
    }

    public BadRequestException(
            String message
    ) {
        super(message);
        this.data = message;
    }

//    public BadRequestException(
//            HttpStatus status,
//            String message
//    ) {
//        this(message);
//        this.status = status;
//    }
//
//    public BadRequestException(
//            HttpStatus status,
//            String message,
//            Object data
//    ) {
//        this(
//                status,
//                message
//        );
//        this.data = data;
//    }

}
