package com.huynv.dentcareWebApi.exception;

import com.huynv.dentcareWebApi.dto.response.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class CustomControllerAdvice {
    Logger logger = LoggerFactory.getLogger(CustomControllerAdvice.class);

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleCustomErrorExceptions(
            Exception e
    ) {
        BadRequestException customErrorException = (BadRequestException) e;
        return new ResponseEntity<>(new ResponseData<>(null, Result.BAD_REQUEST, customErrorException.getData()),
                HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<?> handleCustomErrorUnauthorizedException(
            UnauthorizedException e
    ) {
        UnauthorizedException customErrorException = (UnauthorizedException) e;
        return new ResponseEntity<>(new ResponseData<>(null, Result.UNAUTHORIZED, customErrorException.getData()),
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handle(Exception e,
                                         HttpServletRequest request, HttpServletResponse response) {
//        LOGGER.error("Error: ", e.getMessage() || e.get);
        //get detailMessage
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String detailMessage = sw.toString();
        logger.error("Error: ", detailMessage);
        return new ResponseEntity<>(new ResponseData<>(null, Result.BAD_REQUEST, detailMessage),
                HttpStatus.BAD_REQUEST);
    }
}
