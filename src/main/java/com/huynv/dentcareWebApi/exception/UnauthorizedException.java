package com.huynv.dentcareWebApi.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

/**
 * @author sangnk
 */
@Getter
@Setter
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends Exception implements Serializable {
    private String data = null;

    private static final long serialVersionUID = 1668054874585244657L;

    public UnauthorizedException(final String message) {
        super(message);
        this.data = message;
    }
}
