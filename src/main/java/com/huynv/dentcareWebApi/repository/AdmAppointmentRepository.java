package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmAppointment;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface AdmAppointmentRepository extends BaseRepository<AdmAppointment>{


}
