package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmCustomer;
import com.huynv.dentcareWebApi.entity.AdmDoctor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AdmCustomerRepository extends BaseRepository<AdmCustomer>{
    @Query("DELETE FROM AdmCustomer c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);

    @Query("select a from AdmCustomer a where a.admUser.id = ?1")
    Optional<AdmCustomer> findCustomerByUserId(Long id);
}
