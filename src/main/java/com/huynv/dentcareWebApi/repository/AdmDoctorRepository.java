package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmDoctor;
import com.huynv.dentcareWebApi.entity.AdmStaff;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AdmDoctorRepository extends BaseRepository<AdmDoctor>{
    @Query("DELETE FROM AdmDoctor c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);

    @Query("select a from AdmDoctor a where a.admUser.id = ?1")
    Optional<AdmDoctor> findDoctorByUserId(Long id);
}
