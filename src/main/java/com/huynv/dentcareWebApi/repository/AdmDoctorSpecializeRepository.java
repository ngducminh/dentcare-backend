package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmDoctor;
import com.huynv.dentcareWebApi.entity.AdmDoctorSpecialize;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AdmDoctorSpecializeRepository extends BaseRepository<AdmDoctorSpecialize> {

    @Query("SELECT d FROM AdmDoctorSpecialize d WHERE d.admDoctor.id = ?1")
    List<AdmDoctorSpecialize> findByDoctorId(Long id);
}
