package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmEquipment;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdmEquipmentRepository extends BaseRepository<AdmEquipment>{
    @Query("DELETE FROM AdmEquipment c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);
}
