package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmHomePage;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface AdmHomePageRepository extends BaseRepository<AdmHomePage>{
    @Query("DELETE FROM AdmHomePage c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);
}