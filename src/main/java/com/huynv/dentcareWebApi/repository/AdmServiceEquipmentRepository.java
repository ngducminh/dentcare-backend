package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmDoctorSpecialize;
import com.huynv.dentcareWebApi.entity.AdmService;
import com.huynv.dentcareWebApi.entity.AdmServiceEquipment;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdmServiceEquipmentRepository extends BaseRepository<AdmServiceEquipment> {
    @Query("select a from AdmServiceEquipment a where a.admService.id = ?1")
//    List<AdmServiceEquipment> findByAdmService(AdmService admService);
    List<AdmServiceEquipment> findByServiceId(Long id);

}
