package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmService;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface AdmServiceRepository extends  BaseRepository<AdmService>{
    @Query("DELETE FROM AdmService c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);


}
