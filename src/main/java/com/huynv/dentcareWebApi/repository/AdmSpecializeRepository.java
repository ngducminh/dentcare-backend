package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmSpecialize;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdmSpecializeRepository extends BaseRepository<AdmSpecialize>{
    @Query("DELETE FROM AdmSpecialize c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);
}
