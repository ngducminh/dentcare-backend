package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmStaff;
import com.huynv.dentcareWebApi.entity.AdmUser;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AdmStaffRepository extends BaseRepository<AdmStaff>{
    Optional<AdmStaff> findByFullName(String fullName);

    @Query("select a from AdmStaff a where a.admUser.id = ?1")
    Optional<AdmStaff> findStaffByUserId(Long id);

//    Optional<AdmStaff> findByUsername(String username);
    @Query("DELETE FROM AdmStaff c WHERE c.id IN ?1")
    int deleteAllByIds(List<Long> ids);
}
