package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.AdmUser;
//import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface AdmUserRepository extends BaseRepository<AdmUser> {
    Optional<AdmUser> findByUsername(String username);
//    Optional<AdmUser> findByEmail(String email);

//    @Query()
//    AdmUser findByUserName(String username);


    List<AdmUser> findByIdIn(List<Long> userIds);
}
