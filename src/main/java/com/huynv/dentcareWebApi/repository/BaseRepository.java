package com.huynv.dentcareWebApi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;


@NoRepositoryBean
public interface BaseRepository<E> extends JpaRepository<E, Long> {


    @Query("SELECT e FROM #{#entityName} e WHERE e.isDelete = 0 and e.id = ?1")
    Optional<E> findByIdAndIsDelete(Long id, Long isDelete);

    @Query("SELECT e FROM #{#entityName} e WHERE e.isDelete = 0 and e.id in ?1")
    List<E> findAllByIdsAndIsDelete(List<Long> ids, Long notDeleted);

    //findAllByIsDelete

}
