package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ReportDoanhThuRepository extends JpaRepository<ViewDoanhThu, String> {
    @Query(nativeQuery = true, value = "SELECT * FROM (" +
            "SELECT \n" +
            "  CONCAT(YEAR(a.bookingDate), '-', MONTH(a.bookingDate)) AS thoi_gian, \n" +
            "  COALESCE(SUM(s.price), 0) AS doanh_thu \n" +
            "FROM \n" +
            "  adm_appointment a\n" +
            "  LEFT JOIN adm_appointment_service aps ON a.id = aps.appointment_id\n" +
            "  LEFT JOIN adm_service s ON aps.service_id = s.id\n" +
            "WHERE \n" +
            "  (a.bookingDate BETWEEN :from AND :to) AND\n" +
            " (a.status = 2 OR a.status = 3)" +
            "GROUP BY \n" +
            "  CONCAT(YEAR(a.bookingDate), '-', MONTH(a.bookingDate))" +
            ") as table1")
    List<ViewDoanhThu> tinhDoanhThu(Date from, Date to);

}
