package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ReportDoanhThuTheoDichVuRepository extends JpaRepository<ViewDoanhThu1, String> {

    @Query(nativeQuery = true, value = "SELECT * FROM (\n" +
            "  SELECT \n" +
            "    ROW_NUMBER() OVER() AS id,  \n"+
            "    CONCAT(YEAR(a.bookingDate), '-', MONTH(a.bookingDate)) AS thoi_gian, \n" +
            "    s.id as loai_dich_vu,\n" +
            "    s.name as ten_dich_vu,\n" +
            "    COALESCE(SUM(s.price), 0) AS doanh_thu \n" +
            "  FROM \n" +
            "    adm_appointment a\n" +
            "    LEFT JOIN adm_appointment_service aps ON a.id = aps.appointment_id\n" +
            "    LEFT JOIN adm_service s ON aps.service_id = s.id\n" +
            "  WHERE \n" +
            "   (a.bookingDate BETWEEN :from AND :to) AND (a.status = 2 OR a.status = 3) AND ( :userId is null or a.customer_id = :userId)  \n" +
            "  GROUP BY \n" +
            "    CONCAT(YEAR(a.bookingDate), '-', MONTH(a.bookingDate)),\n" +
            "    s.id\n" +
            ") AS table1")
    List<ViewDoanhThu1> tinhDoanhThuTheoDichVu(Long userId, Date from, Date to);


}
