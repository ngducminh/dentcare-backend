package com.huynv.dentcareWebApi.repository;

import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ReportTheoTrangThaiRepository extends JpaRepository<ThongKeTrangThai, String> {


    @Query(nativeQuery = true, value = "SELECT * FROM (\n" +
            "  SELECT \n" +
            "    ROW_NUMBER() OVER() AS id,  \n"+
            "    CONCAT(YEAR(a.bookingDate), '-', MONTH(a.bookingDate)) AS thoi_gian, \n" +
            "    a.status,\n" +
            "    COUNT(a.status) AS tong_lich_hen \n" +
            "  FROM \n" +
            "    adm_appointment a\n" +
            "  WHERE \n" +
            "   (:userId is null or a.customer_id = :userId) AND (a.bookingDate BETWEEN :from AND :to) \n" +
            "  GROUP BY \n" +
            "    CONCAT(YEAR(a.bookingDate), '-', MONTH(a.bookingDate)),\n" +
            "    a.status\n" +
            ") AS table1")
    List<ThongKeTrangThai> thongkeTrangThai(Long userId, Date from, Date to);
}
