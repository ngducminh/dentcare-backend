package com.huynv.dentcareWebApi.security;


//import com.huynv.dentcareWebApi.entity.AdmAuthority;
//import com.huynv.dentcareWebApi.entity.AdmGroup;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.repository.AdmUserRepository;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: write you class description here
 *
 * @author
 */

@Service
@Transactional
public class CrmUserDetailsService implements UserDetailsService {

    @Autowired
    private AdmUserRepository userRepository;
    @Autowired
    private MessageSource messageSource;

    @Override
    public UserDetails loadUserByUsername(String key) throws UsernameNotFoundException {
        AdmUser user = new AdmUser();
//        check if key is email
        if (key.contains("@")) {
//            user = userRepository.findByEmail(key)
//                    .orElseThrow(() -> new UsernameNotFoundException(messageSource.getMessage("error.login_fail", null, UtilsCommon.getLocale())));
        } else {
            user = userRepository.findByUsername(key)
                    .orElseThrow(() -> new UsernameNotFoundException(messageSource.getMessage("error.login_fail", null, UtilsCommon.getLocale())));
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
//        List<AdmGroup> groups = user.getGroups();
//        for (AdmGroup group : groups) {
//            List<AdmAuthority> authoritiesInGroup = group.getAuthorities();
//            for (AdmAuthority authority : authoritiesInGroup) {
//                authorities.add(new SimpleGrantedAuthority(authority.getAuthority()));
//            }
//        }
        user.setGrantedAuths(authorities);

        return user;
    }

}
