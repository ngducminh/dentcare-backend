package com.huynv.dentcareWebApi.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.response.JwtAuthDto;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.repository.AdmUserRepository;
import com.huynv.dentcareWebApi.service.AdmUserService;
import com.huynv.dentcareWebApi.utils.UtilsHttp;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * TODO: write you class description here
 *
 * @author
 */
@Slf4j
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    @Autowired CrmUserDetailsService userDetailService;
    @Autowired private TokenHelper tokenHelper;
    @Autowired private AdmUserRepository userRepository;
    @Autowired private AdmUserService userService;
    @Autowired private MessageSource messageSource;
    @Autowired private ObjectMapper mapper;

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        boolean isWebIgroring = false;
        String contextPath = request.getContextPath();
        for (String check : Constants.WEB_IGNORING) {
            isWebIgroring = new AntPathMatcher().match(contextPath + check, request.getRequestURI());
            if (isWebIgroring) {
                break;
            }
        }
        return isWebIgroring;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        System.out.println("url " + request.getRequestURL());
        String accessToken = UtilsHttp.getToken(request);

        // json web token
        if (accessToken != null) {
            String username = tokenHelper.getUsernameFromToken(accessToken);
            if (StringUtils.isNotBlank(username)) {
                Optional<AdmUser> userOpt = this.userService.findByUsername(username);
                if (userOpt.isPresent()) {
                    JwtAuthDto jwtAuthDto = tokenHelper.getJWTInfor(accessToken);
                    AdmUser user = (AdmUser) this.userDetailService.loadUserByUsername(username);
                    TokenBasedAuthentication authentication = new TokenBasedAuthentication(user, true);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        chain.doFilter(request, response);
    }

}
