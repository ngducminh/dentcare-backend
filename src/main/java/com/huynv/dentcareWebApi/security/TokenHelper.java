package com.huynv.dentcareWebApi.security;

import com.huynv.dentcareWebApi.dto.request.LoginRequest;
import com.huynv.dentcareWebApi.dto.response.JwtAuthDto;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.utils.UtilsDate;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;

@Component
public class TokenHelper {

    private static final String USER_ID = "ui";
    private static final String USER_NAME = "uname";
    private static final String US_AGENT = "us-agent";
    private static final String IP = "ip";
    private static final String EXPIRATION = "exp";
    private static final String EMAIL_KEY = "mail";
    private static final String FULLNAME_KEY = "fullname";
    private static final String SESSION = "ss";

    private static final String token_signing_key="me2OiYnkuktbtggjmo8dOayJkCHJv7vu35uEKAAPE0Aw9dtN4zCdltxtSrOh9ca5DrPCwXK1r5LZ479reQ3tbg";
    @Value("${spring.application.name}")
    private String APP_NAME;
    @Value("${jwt.expires_in}")
    private int EXPIRES_IN;
    byte[] secret = token_signing_key.getBytes();

//    @PostConstruct
//    private void postConstruct() {
//        this.publicKey = readPublicKey(publicKeyFile);
//    }
//    public static void main(String[] args) {
//        SecureRandom secureRandom = new SecureRandom();
//        byte[] keyBytes = new byte[64]; // 512 bits / 8 = 64 bytes
//        secureRandom.nextBytes(keyBytes);
//        String key = Base64.getUrlEncoder().withoutPadding().encodeToString(keyBytes);
//        System.out.println(key);
//    }
    public String getUsernameFromToken(String token) {
        String username = "";
        try {
            final Claims claims = this.getClaimsFromToken(token);

            Date validTo = claims.getExpiration();
            Date requestTime = new Date();

            if (validTo.after(requestTime)) {
                username = claims.getSubject();
            }

        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public String generateToken(LoginRequest loginRequest, AdmUser user, String session) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        String jws = Jwts.builder()
                .setIssuer(APP_NAME)
                .setSubject(loginRequest.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret)
//                .signWith(getPrivateSigningKey(), SIGNATURE_ALGORITHM)
                .claim(USER_ID, user.getId().toString())
                .claim(USER_NAME, user.getUsername())
//                .claim(EMAIL_KEY, user.getEmail())
//                .claim(FULLNAME_KEY, user.getName())
                .claim(SESSION, session)
//                .claim(EMAIL_KEY, user.getEmail())
                .claim(IP, loginRequest.getIp())
                .claim(US_AGENT, loginRequest.getUserAgent())
                .compact();
        return jws;
    }

    @SneakyThrows
    public JwtAuthDto getJWTInfor(String authToken) {
        Claims body = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(authToken)
                .getBody();
        return JwtAuthDto.builder()
                .issuer(body.getIssuer())
                .ui(body.get(USER_ID, String.class))
                .expi(body.get(EXPIRATION, Date.class))
                .uname(body.get(USER_NAME, String.class))
                .ipAddress(body.get(IP, String.class))
                .usAgent(body.get(US_AGENT, String.class))
                .email(body.get(EMAIL_KEY, String.class))
                .fullname(body.get(FULLNAME_KEY, String.class))
                .ss(body.get(SESSION, String.class))
                .build();
    }

    public Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

//    public boolean validateToken(String authToken) {
//        try {
//            Jwts.parser().setSigningKey(getPublicSigningKey()).parseClaimsJws(authToken);
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }

//    public Key getPublicSigningKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
//        KeyFactory kf = KeyFactory.getInstance("RSA");
//        PublicKey key = kf.generatePublic(new X509EncodedKeySpec(publicKey));
//        return key;
//    }
//
//    public Key getPrivateSigningKey() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
//        InputStream is = new ClassPathResource(this.privateKeyFile, this.getClass().getClassLoader()).getInputStream();
//        byte[] privateKey = IOUtils.toByteArray(is);
//        KeyFactory kf = KeyFactory.getInstance("RSA");
//        PrivateKey key = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKey));
//        return key;
//    }
//
//    public byte[] readPublicKey(String publicKeyFile) {
//        try {
//            InputStream is = new ClassPathResource(publicKeyFile, this.getClass().getClassLoader()).getInputStream();
//            return publicKey = IOUtils.toByteArray(is);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public Date generateExpirationDate() {
        Date date = UtilsDate.sencondDate(new Date(), EXPIRES_IN);
        return date;
    }

}
