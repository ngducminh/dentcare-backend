package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmEquipment;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AdmEquipmentService {
    Optional<AdmEquipment> save(AdmEquipment entity);
    Optional<AdmEquipment> update(AdmEquipment entity) throws BadRequestException;
    Optional<AdmEquipment> get(Long id) throws BadRequestException;
    Page<AdmEquipment> getPaging(SearchForm search, Pageable pageable);
    List<AdmEquipment> getAll();
    Boolean deleteById(Long id) throws BadRequestException;

    boolean locks(List<Long> ids);

    boolean unlocks(List<Long> ids);

    boolean deleteIds(List<Long> ids);
}
