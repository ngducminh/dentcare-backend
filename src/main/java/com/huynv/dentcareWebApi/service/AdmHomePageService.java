package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.entity.AdmHomePage;
import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import com.huynv.dentcareWebApi.exception.BadRequestException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AdmHomePageService {
    AdmHomePage edit(AdmHomePage form) throws BadRequestException;
    Optional<AdmHomePage> getHomePage(Long id);

    List<ViewDoanhThu> tinhDoanhThu(Date from, Date to);

    List<ViewDoanhThu1> tinhDoanhThuTheoDichVu(Long userId, Date from, Date to);

    List<ThongKeTrangThai> thongkeTrangThai(Long userId, Date from, Date to);
}
