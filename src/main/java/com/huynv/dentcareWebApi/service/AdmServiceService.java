package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmEquipment;
import com.huynv.dentcareWebApi.entity.AdmService;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AdmServiceService {
    Optional<AdmService> save(AdmService entity);

    AdmService addService(AdmService doctor, List<AdmEquipment> specializes, List<Integer> slg) throws BadRequestException;

    //edit Service
    AdmService editSerivce(AdmService doctor, List<AdmEquipment> specializes, List<Integer> slg) throws BadRequestException;

    Optional<AdmService> update(AdmService entity) throws BadRequestException;
    Optional<AdmService> get(Long id) throws BadRequestException;
    Page<AdmService> getPaging(SearchForm search, Pageable pageable);
    List<AdmService> getAll();
    Boolean deleteById(Long id) throws BadRequestException;

    boolean locks(List<Long> ids);

    boolean unlocks(List<Long> ids);

    boolean deleteIds(List<Long> ids);
}
