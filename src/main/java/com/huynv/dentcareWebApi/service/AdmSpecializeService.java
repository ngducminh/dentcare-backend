package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmEquipment;
import com.huynv.dentcareWebApi.entity.AdmSpecialize;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AdmSpecializeService {
    Optional<AdmSpecialize> save(AdmSpecialize entity);
    Optional<AdmSpecialize> update(AdmSpecialize entity) throws BadRequestException;
    Optional<AdmSpecialize> get(Long id) throws BadRequestException;
    Page<AdmSpecialize> getPaging(SearchForm search, Pageable pageable);
    List<AdmSpecialize> getAll();
    Boolean deleteById(Long id) throws BadRequestException;

    boolean locks(List<Long> ids);

    boolean unlocks(List<Long> ids);

    boolean deleteIds(List<Long> ids);
}
