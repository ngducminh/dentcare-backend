package com.huynv.dentcareWebApi.service;


import com.huynv.dentcareWebApi.dto.request.ChangePass;
import com.huynv.dentcareWebApi.dto.request.ResetPass;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.*;
import com.huynv.dentcareWebApi.entity.view.ViewAdmUser;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface AdmUserService {
    Optional<AdmUser> save(AdmUser entity);
    Optional<AdmUser> update(AdmUser entity);
    Optional<AdmUser> get(Long id);
    Page<AdmUser> getPaging(Pageable pageable);
    List<AdmUser> getAll();


    List<AdmDoctor> getAllDoctor();
    List<AdmCustomer> getAllCustomer();

    List<AdmStaff> getAllStaff();
    Boolean deleteById(Long id);

    Optional<AdmUser> findByUsername(String username);
    Optional<AdmUser> changePass(AdmUser form) throws BadRequestException;

    Page getPage(SearchForm searchObject, Pageable pageable);

    //addDoctor
    AdmDoctor addDoctor(AdmDoctor user, List<AdmSpecialize> specializes, List<Integer> experiences) throws BadRequestException;

    //addCustomer
    AdmCustomer addCustomer(AdmCustomer user) throws BadRequestException;

    //addStaff
    AdmStaff addStaff(AdmStaff user) throws BadRequestException;

    //editCustomer
    AdmCustomer editCustomer(AdmCustomer form) throws BadRequestException;

    //editDoctor
    AdmDoctor editDoctor(AdmDoctor doctor, List<AdmSpecialize> specializes, List<Integer> experiences) throws BadRequestException;

    //editStaff
    AdmStaff editStaff(AdmStaff form) throws BadRequestException;

    //deleteCustomer
    boolean deleteCustomer(List<Long> ids);

    //deleteDoctor
    boolean deleteDoctor(List<Long> ids);

    //deleteStaff
    boolean deleteStaff(List<Long> ids);

    Optional<AdmDoctor> getDoctor(Long id);

    Optional<AdmStaff> getStaff(Long id);

    Optional<AdmCustomer> getCustomer(Long id);

    AdmUser edit(AdmUser user) throws BadRequestException;

    AdmUser add(AdmUser user) throws BadRequestException;
    AdmUser findById(Long userId);
    List<AdmUser> list(AdmUser user);

    boolean deleteByIds(List<Long> ids);

    Collection<? extends AdmUser> findByIds(List<Long> userIds);


    AdmUser changePassByAdmin(AdmUser form, Long id) throws BadRequestException;

    boolean locks(List<Long> ids);

    boolean unlocks(List<Long> ids);

    boolean lockDoctors(List<Long> ids);

    boolean unlockDoctors(List<Long> ids);

    boolean lockStaffs(List<Long> ids);

    boolean unlockStaffs(List<Long> ids);

    boolean lockCustomers(List<Long> ids);

    boolean unlockCustomers(List<Long> ids);

    boolean changePassword(ChangePass dto) throws BadRequestException;

    boolean resetPassword(ResetPass dto) throws BadRequestException;

//    boolean admResetPassword(String username);
}
