package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmAppointment;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AppointmentService {
    Optional<AdmAppointment> save(AdmAppointment entity);
    Optional<AdmAppointment> update(AdmAppointment entity) throws BadRequestException;
    Optional<AdmAppointment> get(Long id) throws BadRequestException;
    Page<AdmAppointment> getPaging(SearchForm search, Pageable pageable);
    List<AdmAppointment> getAll();
    Boolean deleteById(Long id) throws BadRequestException;

    boolean deleteIds(List<Long> ids);

    boolean cancels(List<Long> ids);

    boolean approvals(List<Long> ids);

    List<ViewDoanhThu> tinhDoanhThu(Date from, Date to);

    List<ViewDoanhThu1> tinhDoanhThuTheoDichVu(Long userId, Date from, Date to);

    List<ThongKeTrangThai> thongkeTrangThai(Long userId, Date from, Date to);
}
