package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.entity.AdmCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DemoRepo extends JpaRepository<AdmCustomer, Long> {
}
