package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmMedical;
import com.huynv.dentcareWebApi.entity.AdmMedical;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MedicalService {
    Optional<AdmMedical> save(AdmMedical entity);
    Optional<AdmMedical> update(AdmMedical entity) throws BadRequestException;
    Optional<AdmMedical> get(Long id) throws BadRequestException;
    Page<AdmMedical> getPaging(SearchForm search, Pageable pageable);
    List<AdmMedical> getAll();
    Boolean deleteById(Long id) throws BadRequestException;

    boolean deleteIds(List<Long> ids);

}
