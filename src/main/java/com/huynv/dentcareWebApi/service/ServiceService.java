package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmService;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface ServiceService {
    //get page, get by id, create, update, delete
    Page getPage(SearchForm searchObject, Pageable pageable);
    AdmService create(AdmService service);
    boolean delete(List<Long> ids);

}
