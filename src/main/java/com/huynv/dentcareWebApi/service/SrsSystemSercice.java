package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.dto.request.LoginRequest;
import com.huynv.dentcareWebApi.dto.request.UserInfo;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.exception.UnauthorizedException;

public interface SrsSystemSercice {
    UserInfo loginJWT(LoginRequest loginRequest) throws UnauthorizedException, BadRequestException;
}
