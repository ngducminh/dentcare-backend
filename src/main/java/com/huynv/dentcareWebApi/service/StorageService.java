package com.huynv.dentcareWebApi.service;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.entity.Auditable;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.BaseRepository;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class StorageService {
    @Autowired
    private MessageSource messageSource;

    public <DomainType extends Auditable, IDFieldType extends Serializable> DomainType save(
            JpaRepository<DomainType, IDFieldType> repository, DomainType model) {
        AdmUser token = UtilsCommon.getUserLogin().orElse(null);
        populateForSave(model, token);
        return repository.save(model);
    }

    public <DomainType extends Auditable, IDFieldType extends Serializable> List<DomainType> save(
            JpaRepository<DomainType, IDFieldType> repository, List<DomainType> models) {
        AdmUser token = UtilsCommon.getUserLogin().get();
        H.each(models, (index, model) -> populateForSave(model, token));
        return repository.saveAll(models);
    }

    private void populateForSave(Auditable model, AdmUser token) {
        model.setCreateBy(H.isTrue(token) ? token.getUsername() : "");
        model.setCreateById(H.isTrue(token) ? token.getId() : -1L);


        model.setCreatedDate(Calendar.getInstance().getTime());
        this.populateForUpdate(model, token);
    }

    public <DomainType extends Auditable, IDFieldType extends Serializable> List<DomainType> update(
            JpaRepository<DomainType, IDFieldType> repository, List<DomainType> models) {
        AdmUser token = UtilsCommon.getUserLogin().get();
        H.each(models, (index, model) -> populateForUpdate(model, token));
        return repository.saveAll(models);
    }

    /**
     * @param repository
     * @param model
     */
    public <DomainType extends Auditable, IDFieldType extends Serializable> DomainType update(
            JpaRepository<DomainType, IDFieldType> repository, DomainType model) {

        AdmUser token = UtilsCommon.getUserLogin().get();
        populateForUpdate(model, token);
        return repository.save(model);
    }

    private void populateForUpdate(Auditable model, AdmUser token) {
        model.setModifiedBy(H.isTrue(token) ? token.getUsername() : "");
        model.setModifiedById(H.isTrue(token) ? token.getId() : -1L);
        model.setModifiedDate(Calendar.getInstance().getTime());
    }

    /**
     * @param repository
     * @param model
     */
    public <DomainType extends Auditable, IDFieldType extends Serializable> void delete(
            JpaRepository<DomainType, IDFieldType> repository, DomainType model) {
        repository.delete(model);
    }

    //get by id
    public <DomainType extends Auditable> DomainType getById(
            BaseRepository<DomainType> repository, Long id) throws BadRequestException {
//        return repository.findById(id).orElseThrow(() -> new BadRequestException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"Dữ liệu"}, UtilsCommon.getLocale())));
            //find by id and isDelete
        return repository.findByIdAndIsDelete(id, Constants.IS_DELETE.NOT_DELETED).orElseThrow(() -> new BadRequestException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"Dữ liệu"}, UtilsCommon.getLocale())));

    }

    //get by ids
    public <DomainType extends Auditable> List<DomainType> getByIds(
            BaseRepository<DomainType> repository, List<Long> ids) {
        return repository.findAllByIdsAndIsDelete(ids, Constants.IS_DELETE.NOT_DELETED);
    }
}
