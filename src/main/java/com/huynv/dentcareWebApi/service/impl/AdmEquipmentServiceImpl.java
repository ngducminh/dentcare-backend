package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmEquipment;
import com.huynv.dentcareWebApi.entity.AdmSpecialize;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.AdmEquipmentRepository;
import com.huynv.dentcareWebApi.service.AdmEquipmentService;
import com.huynv.dentcareWebApi.service.StorageService;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.QueryBuilder;
import com.huynv.dentcareWebApi.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class AdmEquipmentServiceImpl implements AdmEquipmentService {
    @Autowired
    private AdmEquipmentRepository admEquipmentRepository;

    @Autowired
    private StorageService storageService;
    @Autowired private EntityManager entityManager;
    @Override
    public Optional<AdmEquipment> save(AdmEquipment entity) {
        return Optional.of(storageService.save(admEquipmentRepository, entity));
    }

    @Override
    public Optional<AdmEquipment> update(AdmEquipment form) throws BadRequestException {
        AdmEquipment bo = storageService.getById(admEquipmentRepository, form.getId());
//        return Optional.of();
        AdmEquipment res = storageService.update(admEquipmentRepository, bo.formToBo(form, bo));
        if (H.isTrue(res)) {
            return Optional.of(res);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AdmEquipment> get(Long id) throws BadRequestException {
        AdmEquipment bo = storageService.getById(admEquipmentRepository, id);
        if (H.isTrue(bo)) {
            return Optional.of(bo);
        }
        return Optional.empty();
    }

    @Override
    public Page<AdmEquipment> getPaging(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmEquipment
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmEquipment> lA = entityManager.createQuery("select u from AdmEquipment u where 1=1 and u.isDelete = 0", AdmEquipment.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmEquipment> list = new ArrayList<>();
        String hql = " from AdmEquipment u  " +
                " where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u ) ", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        // keyword
        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.name", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //name
        if (H.isTrue(search.getName())) {
            builder.and(QueryUtils.LIKE, "u.name", "%" + search.getName().trim() + "%");
        }
        // price
        if (H.isTrue(search.getPrice())) {
            builder.and(QueryUtils.EQ, "u.price",  search.getPrice());
        }
        // type
        if (H.isTrue(search.getType())) {
            builder.and(QueryUtils.EQ, "u.type",  search.getType());
        }
        // status
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status",  search.getStatus());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmEquipment.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    @Override
    public List<AdmEquipment> getAll() {
        return admEquipmentRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) throws BadRequestException {
        AdmEquipment medical = storageService.getById(admEquipmentRepository, id);
        if (H.isTrue(medical)) {
            medical.setIsDelete(Constants.IS_DELETE.DELETED);
            storageService.save(admEquipmentRepository, medical);
            return true;
        }
        return false;
    }

    @Override
    public boolean locks(List<Long> ids) {
        List<AdmEquipment> users = storageService.getByIds(admEquipmentRepository, ids).stream().map(user -> {
            user.setStatus(Constants.STATUS.LOCK);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admEquipmentRepository, users);
        return true;
    }

    @Override
    public boolean unlocks(List<Long> ids) {
        List<AdmEquipment> users = storageService.getByIds(admEquipmentRepository, ids).stream().map(user -> {
            user.setStatus(Constants.STATUS.ACTIVE);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admEquipmentRepository, users);
        return true;
    }

    @Override
    public boolean deleteIds(List<Long> ids) {
        List<AdmEquipment> medicals = storageService.getByIds(admEquipmentRepository, ids).stream().map(item -> {
            item.setIsDelete(Constants.IS_DELETE.DELETED);
            return item;
        }).collect(Collectors.toList());
        storageService.update(admEquipmentRepository, medicals);
        return true;
    }
}
