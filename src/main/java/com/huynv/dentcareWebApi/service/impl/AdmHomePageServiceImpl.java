package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.entity.AdmDoctor;
import com.huynv.dentcareWebApi.entity.AdmHomePage;
import com.huynv.dentcareWebApi.entity.AdmStaff;
import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.AdmHomePageRepository;
import com.huynv.dentcareWebApi.repository.AdmUserRepository;
import com.huynv.dentcareWebApi.service.AdmHomePageService;
import com.huynv.dentcareWebApi.service.AppointmentService;
import com.huynv.dentcareWebApi.service.ServiceService;
import com.huynv.dentcareWebApi.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class AdmHomePageServiceImpl extends BaseServiceImpl<AdmHomePage, AdmHomePageRepository>  implements AdmHomePageService {
    public AdmHomePageServiceImpl(AdmHomePageRepository repository) {
        super(repository);
    }

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private ServiceService serviceService;

    private final static Logger LOG = LoggerFactory.getLogger(AdmUserServiceImpl.class);

    @Autowired
    private StorageService storageService;
    @Autowired
    private AdmHomePageRepository admHomePageRepository;

    @Override
    public AdmHomePage edit(AdmHomePage form) throws BadRequestException {
        AdmHomePage homePage = storageService.getById(admHomePageRepository, form.getId());
        return storageService.update(admHomePageRepository, homePage.formToBo(form, homePage));
    }

    @Override
    public Optional<AdmHomePage> getHomePage(Long id) {
        AdmHomePage homePage = admHomePageRepository.findById(id).orElse(null);
        return Optional.ofNullable(homePage);
    }

    @Override
    public List<ViewDoanhThu> tinhDoanhThu(Date from, Date to) {
        return appointmentService.tinhDoanhThu(from, to);
    }

    @Override
    public List<ViewDoanhThu1> tinhDoanhThuTheoDichVu(Long userId, Date from, Date to) {
        return appointmentService.tinhDoanhThuTheoDichVu(userId, from, to);
    }

    @Override
    public List<ThongKeTrangThai> thongkeTrangThai(Long userId, Date from, Date to) {
        return appointmentService.thongkeTrangThai(userId, from, to);
    }

}
