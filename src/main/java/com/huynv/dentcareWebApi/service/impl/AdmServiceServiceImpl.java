package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.*;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.AdmServiceEquipmentRepository;
import com.huynv.dentcareWebApi.repository.AdmServiceRepository;
import com.huynv.dentcareWebApi.service.AdmServiceService;
import com.huynv.dentcareWebApi.service.StorageService;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.QueryBuilder;
import com.huynv.dentcareWebApi.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class AdmServiceServiceImpl implements AdmServiceService {
    @Autowired
    private AdmServiceRepository admServiceRepository;
    @Autowired
    private AdmServiceEquipmentRepository admServiceEquipmentRepository;
    @Autowired
    private StorageService storageService;
    @Autowired private EntityManager entityManager;
    @Override
    public Optional<AdmService> save(AdmService entity) {
        return Optional.of(storageService.save(admServiceRepository, entity));
    }

    @Override
    public AdmService addService(AdmService service, List<AdmEquipment> equipments, List<Integer> slg) throws BadRequestException {
        AdmService saveService = storageService.save(admServiceRepository, service);
        for (int i = 0; i < equipments.size(); i++) {
            AdmEquipment equipment = equipments.get(i);
            Integer quanl = slg.get(i);
            AdmServiceEquipment admServiceEquipment = new AdmServiceEquipment();
            admServiceEquipment.setAdmService(saveService);
            admServiceEquipment.setAdmEquipment(equipment);
            admServiceEquipment.setQuantity(quanl);
            storageService.save(admServiceEquipmentRepository, admServiceEquipment);
        }
        return saveService;
    }
    
    //edit Service
    @Override
    public AdmService editSerivce(AdmService service, List<AdmEquipment> equipments, List<Integer> slg) throws BadRequestException {
        AdmService exits = storageService.getById(admServiceRepository, service.getId());
        exits = service.formToBo(service, exits);

        //remove old AdmServiceEquipment if exists
        removeServiceEquipment(exits);
        for (int i = 0; i < equipments.size(); i++) {
            AdmEquipment equipment  = equipments.get(i);
            Integer quanl = slg.get(i);
            AdmServiceEquipment admServiceEquipment = new AdmServiceEquipment();
            admServiceEquipment.setAdmService(exits);
            admServiceEquipment.setAdmEquipment(equipment);
            admServiceEquipment.setQuantity(quanl);
            storageService.save(admServiceEquipmentRepository, admServiceEquipment);
        }

        return storageService.update(admServiceRepository, exits);
    }
    
    void removeServiceEquipment(AdmService admService) {
        List<AdmServiceEquipment> serviceEquipments = admServiceEquipmentRepository.findByServiceId(admService.getId());
        for(AdmServiceEquipment item : serviceEquipments) {
            admServiceEquipmentRepository.delete(item);
        }
    }
    

    @Override
    public Optional<AdmService> update(AdmService form) throws BadRequestException {
        AdmService bo = storageService.getById(admServiceRepository, form.getId());
//        return Optional.of();
        AdmService res = storageService.update(admServiceRepository, bo.formToBo(form, bo));
        if (H.isTrue(res)) {
            return Optional.of(res);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AdmService> get(Long id) throws BadRequestException {
        AdmService bo = storageService.getById(admServiceRepository, id);
        if (H.isTrue(bo)) {
            return Optional.of(bo);
        }
        return Optional.empty();
    }

    @Override
    public Page<AdmService> getPaging(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmService
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmService> lA = entityManager.createQuery("select u from AdmService u where 1=1 and u.isDelete = 0", AdmService.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmService> list = new ArrayList<>();
        String hql = " from AdmService u left join u.admServiceEquipments ase left join ase.admEquipment ade" +
                " left join u.doctors do_"+
                " where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u ) ", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        // keyword
        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.name", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //id
        if (H.isTrue(search.getId())) {
            builder.and(QueryUtils.EQ, "u.id", search.getId());
        }
//        // equipments
//        if (H.isTrue(search.getEquipmentIds())) {
//            builder.and(QueryUtils.IN, "ade.id", search.getEquipmentIds());
//        }
        // doctorId
        if (H.isTrue(search.getDoctorId())) {
            builder.and(QueryUtils.EQ, "do_.id", search.getDoctorId());
        }
        // equipment
        if (H.isTrue(search.getEquipmentId())) {
            builder.and(QueryUtils.IN, "ade.id", search.getEquipmentId());
        }
        // name
        if (H.isTrue(search.getName())) {
            builder.and(QueryUtils.LIKE,"u.name", "%" + search.getName().trim() + "%");
        }
        // price
        if (H.isTrue(search.getPrice())) {
            builder.and(QueryUtils.EQ, "u.price",  search.getPrice());
        }
        // type
        if (H.isTrue(search.getType())) {
            builder.and(QueryUtils.EQ, "u.type",  search.getType());
        }
        // status
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status",  search.getStatus());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmService.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    @Override
    public List<AdmService> getAll() {
        return admServiceRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) throws BadRequestException {
        AdmService admAppointment = storageService.getById(admServiceRepository, id);
        if (H.isTrue(admAppointment)) {
            admAppointment.setIsDelete(Constants.IS_DELETE.DELETED);
            storageService.save(admServiceRepository, admAppointment);
            return true;
        }
        return false;
    }

    @Override
    public boolean locks(List<Long> ids) {
        List<AdmService> users = storageService.getByIds(admServiceRepository, ids).stream().map(user -> {
            user.setStatus(Constants.STATUS.LOCK);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admServiceRepository, users);
        return true;
    }

    @Override
    public boolean unlocks(List<Long> ids) {
        List<AdmService> users = storageService.getByIds(admServiceRepository, ids).stream().map(user -> {
            user.setStatus(Constants.STATUS.ACTIVE);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admServiceRepository, users);
        return true;
    }

    @Override
    public boolean deleteIds(List<Long> ids) {
        List<AdmService> appointments = storageService.getByIds(admServiceRepository, ids).stream().map(item -> {
            item.setIsDelete(Constants.IS_DELETE.DELETED);
            return item;
        }).collect(Collectors.toList());
        storageService.update(admServiceRepository, appointments);
        return true;
    }
}
