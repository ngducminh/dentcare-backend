package com.huynv.dentcareWebApi.service.impl;


import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.ChangePass;
import com.huynv.dentcareWebApi.dto.request.ResetPass;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.*;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.*;
import com.huynv.dentcareWebApi.service.AdmUserService;
import com.huynv.dentcareWebApi.service.StorageService;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.QueryBuilder;
import com.huynv.dentcareWebApi.utils.QueryUtils;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class AdmUserServiceImpl extends BaseServiceImpl<AdmUser, AdmUserRepository> implements AdmUserService {
    public AdmUserServiceImpl(AdmUserRepository repository) {
        super(repository);
    }

    private final static Logger LOG = LoggerFactory.getLogger(AdmUserServiceImpl.class);

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private AdmUserRepository admUserRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AdmDoctorRepository admDoctorRepository;
    @Autowired
    private AdmStaffRepository admStaffRepository;
    @Autowired
    private AdmCustomerRepository admCustomerRepository;
    @Autowired
    private AdmDoctorSpecializeRepository admDoctorSpecializeRepository;

    @Override
    public Page getPage(SearchForm search, Pageable pageable) {
        Page page = null;
        try {
            if (search.getSearchType().equals(4L)) {
                page = laydanhsachtaikhoan(search, pageable);
            } else if (search.getSearchType().equals(1L)) {
                page = laydanhsachnhanvien(search, pageable);
            } else if (search.getSearchType().equals(2L)) {
                page = laydanhsachkhachhang(search, pageable);
            } else {
                page = laydanhsachbacsi(search, pageable);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return page;
    }

    private Page laydanhsachtaikhoan(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmUser
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmUser> lA = entityManager.createQuery("select u from AdmUser u where 1=1 and u.isDelete = 0", AdmUser.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmUser> list = new ArrayList<>();
        String hql = " from AdmUser u   " +
                "where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u)", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        List<QueryBuilder.ConditionObject> conditionObjects = new ArrayList<>();
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.ACTIVE));
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.LOCK));
        builder.andOrListNative(conditionObjects);

        //keyword
        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.username", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //username
        if (H.isTrue(search.getUsername())) {
            builder.and(QueryUtils.LIKE, "u.username", "%" + search.getUsername().trim() + "%");
        }
        // permission
        if (H.isTrue(search.getPermission())) {
            builder.and(QueryUtils.EQ, "u.permission", search.getPermission());
        }
        //status
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status", search.getStatus());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmUser.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    private Page laydanhsachnhanvien(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmStaff
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmStaff> lA = entityManager.createQuery("select u from AdmStaff u where 1=1 and u.isDelete = 0", AdmStaff.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmStaff> list = new ArrayList<>();
        String hql = " from AdmStaff u    join u.admUser ase " +
                "where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u)", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        List<QueryBuilder.ConditionObject> conditionObjects = new ArrayList<>();
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.ACTIVE));
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.LOCK));
        builder.andOrListNative(conditionObjects);

        //keyword
        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.staffCode", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.email", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.phoneNumber", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE, "ase.username", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //staffCode
        if (H.isTrue(search.getStaffCode())) {
            builder.and(QueryUtils.LIKE, "u.staffCode", "%" + search.getStaffCode().trim() + "%");
        }
        // fullName
        if (H.isTrue(search.getFullName())) {
            builder.and(QueryUtils.LIKE, "u.fullName", "%" + search.getFullName().trim() + "%");
        }
        // username
        if (H.isTrue(search.getUsername())) {
            builder.and(QueryUtils.LIKE, "ase.username", "%" + search.getUsername().trim() + "%");
        }
        //email
        if (H.isTrue(search.getEmail())) {
            builder.and(QueryUtils.LIKE, "u.email", "%" + search.getEmail().trim() + "%");
        }
        //phoneNumber
        if (H.isTrue(search.getPhoneNumber())) {
            builder.and(QueryUtils.LIKE, "u.phoneNumber", "%" + search.getPhoneNumber().trim() + "%");
        }
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status", search.getStatus());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmStaff.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    private Page laydanhsachbacsi(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmDoctor
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmDoctor> lA = entityManager.createQuery("select u from AdmDoctor u where 1=1 and u.isDelete = 0", AdmDoctor.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmDoctor> list = new ArrayList<>();
        String hql = " from AdmDoctor u left join u.admUser ase "+
                " left join u.admDoctorSpecializes do_sp " +
                " left join do_sp.admSpecialize sp_ " +
                "where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u)", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        List<QueryBuilder.ConditionObject> conditionObjects = new ArrayList<>();
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.ACTIVE));
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.LOCK));
        builder.andOrListNative(conditionObjects);

        //keyword
        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.doctorCode", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.email", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.phoneNumber", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE, "ase.username", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //doctorCode
        if (H.isTrue(search.getDoctorCode())) {
            builder.and(QueryUtils.LIKE, "u.doctorCode", "%" + search.getDoctorCode().trim() + "%");
        }
        // fullName
        if (H.isTrue(search.getFullName())) {
            builder.and(QueryUtils.LIKE, "u.fullName", "%" + search.getFullName().trim() + "%");
        }
        //email
        if (H.isTrue(search.getEmail())) {
            builder.and(QueryUtils.LIKE, "u.email", "%" + search.getEmail().trim() + "%");
        }
        //phoneNumber
        if (H.isTrue(search.getPhoneNumber())) {
            builder.and(QueryUtils.LIKE, "u.phoneNumber", "%" + search.getPhoneNumber().trim() + "%");
        }
        // username
        if (H.isTrue(search.getUsername())) {
            builder.and(QueryUtils.LIKE, "ase.username", "%" + search.getUsername().trim() + "%");
        }
        // specializeId
        if (H.isTrue(search.getSpecializeId())) {
            builder.and(QueryUtils.EQ, "sp_.id", search.getSpecializeId() );
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        //status
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status", search.getStatus());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmDoctor.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    private Page laydanhsachkhachhang(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmCustomer
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmCustomer> lA = entityManager.createQuery("select u from AdmCustomer u where 1=1 and u.isDelete = 0", AdmCustomer.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmCustomer> list = new ArrayList<>();
        String hql = " from AdmCustomer u  join u.admUser ase " +
                "where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u)", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        List<QueryBuilder.ConditionObject> conditionObjects = new ArrayList<>();
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.ACTIVE));
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.LOCK));
        builder.andOrListNative(conditionObjects);

        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.customerCode", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.email", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.phoneNumber", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE, "ase.username", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //customerCode
        if (H.isTrue(search.getCustomerCode())) {
            builder.and(QueryUtils.LIKE, "u.customerCode", "%" + search.getCustomerCode().trim() + "%");
        }
        // fullName
        if (H.isTrue(search.getFullName())) {
            builder.and(QueryUtils.LIKE, "u.fullName", "%" + search.getFullName().trim() + "%");
        }
        // username
        if (H.isTrue(search.getUsername())) {
            builder.and(QueryUtils.LIKE, "ase.username", "%" + search.getUsername().trim() + "%");
        }
        //email
        if (H.isTrue(search.getEmail())) {
            builder.and(QueryUtils.LIKE, "u.email", "%" + search.getEmail().trim() + "%");
        }
        //phoneNumber
        if (H.isTrue(search.getPhoneNumber())) {
            builder.and(QueryUtils.LIKE, "u.phoneNumber", "%" + search.getPhoneNumber().trim() + "%");
        }
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status", search.getStatus());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmCustomer.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    @Override
    public AdmUser add(AdmUser user) throws BadRequestException {
        validateUser(user);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        AdmUser userDB = storageService.save(admUserRepository, user);
        return userDB;
    }

    //addDoctor
    @Override
    public AdmDoctor addDoctor(AdmDoctor doctor, List<AdmSpecialize> specializes, List<Integer> experiences) throws BadRequestException {
        AdmDoctor savedDoctor = storageService.save(admDoctorRepository, doctor);
        for (int i = 0; i < specializes.size(); i++) {
            AdmSpecialize specialize = specializes.get(i);
            Integer experience = experiences.get(i);
            AdmDoctorSpecialize doctorSpecialize = new AdmDoctorSpecialize();
            doctorSpecialize.setAdmDoctor(savedDoctor);
            doctorSpecialize.setAdmSpecialize(specialize);
            doctorSpecialize.setExperience(experience);
            storageService.save(admDoctorSpecializeRepository, doctorSpecialize);
        }
        return savedDoctor;
    }



    //addCustomer
    @Override
    public AdmCustomer addCustomer(AdmCustomer user) throws BadRequestException {
        AdmCustomer userDB = storageService.save(admCustomerRepository, user);
        return userDB;
    }

    //addStaff
    @Override
    public AdmStaff addStaff(AdmStaff user) throws BadRequestException {
        AdmStaff admStaff = storageService.save(admStaffRepository, user);
        return admStaff;
    }

    //editCustomer
    @Override
    public AdmCustomer editCustomer(AdmCustomer form) throws BadRequestException {
        AdmCustomer user = storageService.getById(admCustomerRepository, form.getId());
        return storageService.update(admCustomerRepository, user.formToBo(form, user));
    }

    //editDoctor
    @Override
    public AdmDoctor editDoctor(AdmDoctor doctor, List<AdmSpecialize> specializes, List<Integer> experiences) throws BadRequestException {
        AdmDoctor existingDoctor = storageService.getById(admDoctorRepository, doctor.getId());
        existingDoctor = doctor.formToBo(doctor, existingDoctor);

        //remove old AdmDoctorSpecialize if exists
        removeDoctorSpecialize(existingDoctor);
        for (int i = 0; i < specializes.size(); i++) {
            AdmSpecialize specialize = specializes.get(i);
            Integer experience = experiences.get(i);
            AdmDoctorSpecialize doctorSpecialize = new AdmDoctorSpecialize();
            doctorSpecialize.setAdmDoctor(existingDoctor);
            doctorSpecialize.setAdmSpecialize(specialize);
            doctorSpecialize.setExperience(experience);
            storageService.save(admDoctorSpecializeRepository, doctorSpecialize);
        }

        return storageService.update(admDoctorRepository, existingDoctor);
    }

    void removeDoctorSpecialize(AdmDoctor doctor) {
        List<AdmDoctorSpecialize> doctorSpecializes = admDoctorSpecializeRepository.findByDoctorId(doctor.getId());
        for(AdmDoctorSpecialize item : doctorSpecializes) {
            admDoctorSpecializeRepository.delete(item);
        }
    }

    //editStaff
    @Override
    public AdmStaff editStaff(AdmStaff form) throws BadRequestException {
        AdmStaff user = storageService.getById(admStaffRepository, form.getId());
        return storageService.update(admStaffRepository, user.formToBo(form, user));
    }

    //deleteCustomer
    @Override
    public boolean deleteCustomer(List<Long> ids) {
        List<AdmCustomer> users = storageService.getByIds(admCustomerRepository, ids).stream().map(user -> {
            user.setIsDelete(Constants.IS_DELETE.DELETED);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admCustomerRepository, users);
        return true;
    }

    //deleteDoctor
    @Override
    public boolean deleteDoctor(List<Long> ids) {
        List<AdmDoctor> users = storageService.getByIds(admDoctorRepository, ids).stream().map(user -> {
            user.setIsDelete(Constants.IS_DELETE.DELETED);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admDoctorRepository, users);
        return true;
    }

    //deleteStaff
    @Override
    public boolean deleteStaff(List<Long> ids) {
        List<AdmStaff> users = storageService.getByIds(admStaffRepository, ids).stream().map(user -> {
            user.setIsDelete(Constants.IS_DELETE.DELETED);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admStaffRepository, users);
        return true;
    }

    @Override
    public Optional<AdmDoctor> getDoctor(Long id) {
        AdmDoctor doctor = admDoctorRepository.findById(id).orElse(null);
        return Optional.ofNullable(doctor);
    }

    @Override
    public Optional<AdmStaff> getStaff(Long id) {
        AdmStaff staff = admStaffRepository.findById(id).orElse(null);
        return Optional.ofNullable(staff);
    }

    @Override
    public Optional<AdmCustomer> getCustomer(Long id) {
        AdmCustomer customer = admCustomerRepository.findById(id).orElse(null);
        return Optional.ofNullable(customer);
    }


    private void validateUser(AdmUser user) throws BadRequestException {
        if (H.isTrue(user.getUsername())) {
            AdmUser admUser = admUserRepository.findByUsername(user.getUsername()).orElse(null);
            if (admUser != null && !admUser.getId().equals(user.getId())) {
                throw new BadRequestException(messageSource.getMessage("error.USERNAME_EXISTED", new Object[]{user.getUsername()}, UtilsCommon.getLocale()));
            }
        }
    }

//    public AdmStaff editStaff(AdmStaff form) throws BadRequestException {
//        AdmStaff user = storageService.getById(admStaffRepository, form.getId());
//        return storageService.update(admStaffRepository, user.formToBo(form, user));
//    }

    @Override
    public AdmUser edit(AdmUser form) throws BadRequestException {
        validateUser(form);
//        AdmUser user = get(form.getId()).orElseThrow(
//                () -> new BadRequestException(messageSource.getMessage("error.ENTITY_NOT_FOUND", new Object[]{"AdmUser"}, UtilsCommon.getLocale()))
//        );
//        return update(user.formToBo(form, user));
        AdmUser user = storageService.getById(admUserRepository, form.getId());
        return storageService.update(admUserRepository, user.formToBo(form, user));
    }

    public List<AdmUser> list(AdmUser user) {
        return admUserRepository.findAll();
    }

    @Override
    public Optional<AdmUser> get(Long id) {
        AdmUser user = admUserRepository.findById(id).orElse(null);
        return Optional.ofNullable(user);

    }

    @Override
    public List<AdmDoctor> getAllDoctor() {
        return admDoctorRepository.findAll();
    }

    @Override
    public List<AdmCustomer> getAllCustomer() {
        return admCustomerRepository.findAll();
    }

    @Override
    public List<AdmStaff> getAllStaff() {
        return admStaffRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) {
        AdmUser user = admUserRepository.findById(id).orElse(null);
        if (user != null) {
            user.setIsDelete(Constants.IS_DELETE.DELETED);
            admUserRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteByIds(List<Long> ids) {
        List<AdmUser> users = storageService.getByIds(admUserRepository, ids).stream().map(user -> {
            user.setIsDelete(Constants.IS_DELETE.DELETED);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admUserRepository, users);
        return true;
    }

    @Override
    public List<AdmUser> findByIds(List<Long> userIds) {
        return storageService.getByIds(admUserRepository, userIds);
    }


    @Override
    public AdmUser changePassByAdmin(AdmUser form, Long id) throws BadRequestException {
        AdmUser user = storageService.getById(admUserRepository, id);
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        return storageService.update(admUserRepository, user);
    }

    @Override
    public boolean locks(List<Long> ids) {
        List<AdmUser> users = storageService.getByIds(admUserRepository, ids).stream().map(user -> {
            user.setStatus(Constants.STATUS.LOCK);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admUserRepository, users);
        return true;
    }

    @Override
    public boolean unlocks(List<Long> ids) {
        List<AdmUser> users = storageService.getByIds(admUserRepository, ids).stream().map(user -> {
            user.setStatus(Constants.STATUS.ACTIVE);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admUserRepository, users);
        return true;
    }

    @Override
    public boolean lockDoctors(List<Long> ids) {
        List<AdmDoctor> doctors = storageService.getByIds(admDoctorRepository, ids).stream().map(doctor -> {
            doctor.setStatus(Constants.STATUS.LOCK);
            return doctor;
        }).collect(Collectors.toList());
        storageService.update(admDoctorRepository, doctors);
        return true;
    }

    @Override
    public boolean unlockDoctors(List<Long> ids) {
        List<AdmDoctor> doctors = storageService.getByIds(admDoctorRepository, ids).stream().map(doctor -> {
            doctor.setStatus(Constants.STATUS.ACTIVE);
            return doctor;
        }).collect(Collectors.toList());
        storageService.update(admDoctorRepository, doctors);
        return true;
    }

    @Override
    public boolean lockStaffs(List<Long> ids) {
        List<AdmStaff> staffs = storageService.getByIds(admStaffRepository, ids).stream().map(staff -> {
            staff.setStatus(Constants.STATUS.LOCK);
            return staff;
        }).collect(Collectors.toList());
        storageService.update(admStaffRepository, staffs);
        return true;
    }

    @Override
    public boolean unlockStaffs(List<Long> ids) {
        List<AdmStaff> staffs = storageService.getByIds(admStaffRepository, ids).stream().map(staff -> {
            staff.setStatus(Constants.STATUS.ACTIVE);
            return staff;
        }).collect(Collectors.toList());
        storageService.update(admStaffRepository, staffs);
        return true;
    }

    @Override
    public boolean lockCustomers(List<Long> ids) {
        List<AdmCustomer> customers = storageService.getByIds(admCustomerRepository, ids).stream().map(customer -> {
            customer.setStatus(Constants.STATUS.LOCK);
            return customer;
        }).collect(Collectors.toList());
        storageService.update(admCustomerRepository, customers);
        return true;
    }

    @Override
    public boolean unlockCustomers(List<Long> ids) {
        List<AdmCustomer> customers = storageService.getByIds(admCustomerRepository, ids).stream().map(customer -> {
            customer.setStatus(Constants.STATUS.ACTIVE);
            return customer;
        }).collect(Collectors.toList());
        storageService.update(admCustomerRepository, customers);
        return true;
    }

    @Override
    public boolean changePassword(ChangePass dto) throws BadRequestException {
        //compare old pass
        AdmUser userLogin = UtilsCommon.getUserLogin().get();
        if(!passwordEncoder.matches(dto.getOldPass(), userLogin.getPassword())){
            throw new BadRequestException("Mật khẩu cũ không đúng");
        }
        if(!dto.getNewPass().equals(dto.getConfirmPass())){
            throw new BadRequestException("Mật khẩu mới không trùng khớp");
        }
        AdmUser user = storageService.getById(admUserRepository, userLogin.getId());
        user.setPassword(passwordEncoder.encode(dto.getConfirmPass()));
        storageService.update(admUserRepository, user);
        return true;
    }

    @Override
    public boolean resetPassword(ResetPass dto) throws BadRequestException {
        AdmUser userReset = admUserRepository.findByUsername(dto.getUsername()).orElse(null);
        if(userReset != null){
            if(dto.getType()==0){
                AdmStaff admStaff = admStaffRepository.findStaffByUserId(userReset.getId()).orElse(null);
                if(admStaff!=null && !dto.getEmail().equals(admStaff.getEmail())){
                    throw new BadRequestException("Email không chính xác");
                }
                if(admStaff!=null && !dto.getPhoneNumber().equals(admStaff.getPhoneNumber())){
                    throw new BadRequestException("Số điện thoại không chính xác");
                }
                if(admStaff==null){
                    throw new BadRequestException("username không chính xác");
                }
            }else if(dto.getType()==1){
                AdmDoctor admDoctor = admDoctorRepository.findDoctorByUserId(userReset.getId()).orElse(null);
                if(admDoctor!=null && !dto.getEmail().equals(admDoctor.getEmail())){
                    throw new BadRequestException("Email không chính xác");
                }
                if(admDoctor!=null && !dto.getPhoneNumber().equals(admDoctor.getPhoneNumber())){
                    throw new BadRequestException("Số điện thoại không chính xác");
                }
                if(admDoctor==null){
                    throw new BadRequestException("username không chính xác");
                }
            }else{
                AdmCustomer admCustomer = admCustomerRepository.findCustomerByUserId(userReset.getId()).orElse(null);
                if(admCustomer!=null && !dto.getEmail().equals(admCustomer.getEmail())){
                    throw new BadRequestException("Email không chính xác");
                }
                if(admCustomer!=null && !dto.getPhoneNumber().equals(admCustomer.getPhoneNumber())){
                    throw new BadRequestException("Số điện thoại không chính xác");
                }
                if(admCustomer==null){
                    throw new BadRequestException("username không chính xác");
                }
            }
            userReset.setPassword(passwordEncoder.encode("123456aA@"));
            admUserRepository.save(userReset);
            return true;
        }else{
            throw new BadRequestException("username không chính xác");
        }
    }
//
//    @Override
//    public boolean admResetPassword(String username) {
//        AdmUser user = null;
//        try {
//            user = admUserRepository.findByUserName(username);
//            if(user == null) {
//                throw new BadRequestException("username không chính xác");
//            }
//
//            user.setPassword(passwordEncoder.encode("123456aA@"));
//
//            admUserRepository.save(user);
//
//            return true;
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//        return false;
//    }


    @Override
    public AdmUser findById(Long userId) {
        return admUserRepository.findById(userId).orElse(null);
    }


    @Override
    public Optional<AdmUser> findByUsername(String username) {
        return admUserRepository.findByUsername(username);
    }

    @Override
    public Optional<AdmUser> changePass(AdmUser form) throws BadRequestException {
        AdmUser userLogin = UtilsCommon.getUserLogin().get();
        if (userLogin.getId().equals(form.getId())) {
            AdmUser user = storageService.getById(admUserRepository, form.getId());
            user.setPassword(passwordEncoder.encode(form.getPassword()));
            return Optional.ofNullable(storageService.update(admUserRepository, user));
        } else {
            throw new BadRequestException(messageSource.getMessage("error_401", new Object[]{form.getId()}, UtilsCommon.getLocale()));
        }
    }
}
