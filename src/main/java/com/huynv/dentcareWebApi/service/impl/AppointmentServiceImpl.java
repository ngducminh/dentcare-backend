package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmAppointment;
import com.huynv.dentcareWebApi.entity.AdmService;
import com.huynv.dentcareWebApi.entity.view.ThongKeTrangThai;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.AdmAppointmentRepository;
import com.huynv.dentcareWebApi.repository.ReportDoanhThuRepository;
import com.huynv.dentcareWebApi.repository.ReportDoanhThuTheoDichVuRepository;
import com.huynv.dentcareWebApi.repository.ReportTheoTrangThaiRepository;
import com.huynv.dentcareWebApi.service.AppointmentService;
import com.huynv.dentcareWebApi.service.StorageService;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.QueryBuilder;
import com.huynv.dentcareWebApi.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class AppointmentServiceImpl implements AppointmentService {
    @Autowired
    private AdmAppointmentRepository appointmentRepository;
    @Autowired
    private ReportDoanhThuRepository reportDoanhThuRepository;
    @Autowired
    private ReportDoanhThuTheoDichVuRepository reportDoanhThuTheoDichVuRepository;
    @Autowired
    private ReportTheoTrangThaiRepository reportTheoTrangThaiRepository;
    @Autowired
    private StorageService storageService;
    @Autowired private EntityManager entityManager;
    @Override
    public Optional<AdmAppointment> save(AdmAppointment entity) {
        return Optional.of(storageService.save(appointmentRepository, entity));
    }

    @Override
    public Optional<AdmAppointment> update(AdmAppointment form) throws BadRequestException {
        AdmAppointment bo = storageService.getById(appointmentRepository, form.getId());
//        return Optional.of();
        AdmAppointment res = storageService.update(appointmentRepository, bo.formToBo(form, bo));
        if (H.isTrue(res)) {
            return Optional.of(res);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AdmAppointment> get(Long id) throws BadRequestException {
        AdmAppointment bo = storageService.getById(appointmentRepository, id);
        if (H.isTrue(bo)) {
            return Optional.of(bo);
        }
        return Optional.empty();
    }

    @Override
    public Page<AdmAppointment> getPaging(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmAppointment
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmAppointment> lA = entityManager.createQuery("select u from AdmAppointment u where 1=1 and u.isDelete = 0", AdmAppointment.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmAppointment> list = new ArrayList<>();
        String hql = " from AdmAppointment u  left join u.admDoctor ad_ " +
                " left join u.admStaff as_ left join u.admCustomer ac_ " +
                " left join u.services se_ "+
                " where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u ) ", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);


        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.appointmentCode", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.note", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"as_.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"ad_.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"ac_.fullName", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //appointmentCode
        if (H.isTrue(search.getAppointmentCode())) {
            builder.and(QueryUtils.LIKE, "u.appointmentCode", "%" + search.getAppointmentCode().trim() + "%");
        }
        // customerId
        if (H.isTrue(search.getCustomerId())) {
            builder.and(QueryUtils.EQ, "ac_.id", search.getCustomerId());
        }
        // doctorId
        if (H.isTrue(search.getDoctorId())) {
            builder.and(QueryUtils.EQ, "ad_.id", search.getDoctorId());
        }
        // serviceId
        if (H.isTrue(search.getServiceId())) {
            builder.and(QueryUtils.IN, "se_.id", search.getServiceId());
        }
        //bookingDateFrom and bookingDateTo
        if (H.isTrue(search.getBookingDateFrom())) {
            builder.and(QueryUtils.GE, "u.bookingDate", search.getBookingDateFrom());
        }
        if (H.isTrue(search.getBookingDateTo())) {
            builder.and(QueryUtils.LE, "u.bookingDate", search.getBookingDateTo());
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        // status
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status",  search.getStatus());
        }


        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmAppointment.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    @Override
    public List<AdmAppointment> getAll() {
        return appointmentRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) throws BadRequestException {
        AdmAppointment admAppointment = storageService.getById(appointmentRepository, id);
        if (H.isTrue(admAppointment)) {
            admAppointment.setIsDelete(Constants.IS_DELETE.DELETED);
            storageService.save(appointmentRepository, admAppointment);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteIds(List<Long> ids) {
        List<AdmAppointment> appointments = storageService.getByIds(appointmentRepository, ids).stream().map(item -> {
            item.setIsDelete(Constants.IS_DELETE.DELETED);
            return item;
        }).collect(Collectors.toList());
        storageService.update(appointmentRepository, appointments);
        return true;
    }

    @Override
    public boolean cancels(List<Long> ids) {
        List<AdmAppointment> users = storageService.getByIds(appointmentRepository, ids).stream().map(user -> {
            if(user.getStatus()==Constants.APPOINTMENT_TYPE.CHO_DUYET||user.getStatus()==Constants.APPOINTMENT_TYPE.DA_DUYET){
                user.setStatus(Constants.APPOINTMENT_TYPE.HUY_LICH);
            }
            return user;
        }).collect(Collectors.toList());
        storageService.update(appointmentRepository, users);
        return true;
    }

    @Override
    public boolean approvals(List<Long> ids) {
        List<AdmAppointment> users = storageService.getByIds(appointmentRepository, ids).stream().map(user -> {
            if(user.getStatus()==Constants.APPOINTMENT_TYPE.CHO_DUYET){
                user.setStatus(Constants.APPOINTMENT_TYPE.DA_DUYET);
            }
            return user;
        }).collect(Collectors.toList());
        storageService.update(appointmentRepository, users);
        return true;
    }

    @Override
    public List<ViewDoanhThu> tinhDoanhThu(Date from, Date to) {
        return reportDoanhThuRepository.tinhDoanhThu(from, to);
    }

    @Override
    public List<ViewDoanhThu1> tinhDoanhThuTheoDichVu(Long userId, Date from, Date to) {
        return reportDoanhThuTheoDichVuRepository.tinhDoanhThuTheoDichVu(userId, from, to);
    }

    @Override
    public List<ThongKeTrangThai> thongkeTrangThai(Long userId, Date from, Date to) {
        return reportTheoTrangThaiRepository.thongkeTrangThai(userId, from, to);
    }

}
