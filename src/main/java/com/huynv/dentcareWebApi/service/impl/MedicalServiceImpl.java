package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmMedical;
import com.huynv.dentcareWebApi.entity.AdmMedical;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.repository.AdmMedicalRepository;
import com.huynv.dentcareWebApi.repository.ReportDoanhThuRepository;
import com.huynv.dentcareWebApi.service.MedicalService;
import com.huynv.dentcareWebApi.service.StorageService;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.QueryBuilder;
import com.huynv.dentcareWebApi.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class MedicalServiceImpl implements MedicalService {
    @Autowired
    private AdmMedicalRepository admMedicalRepository;
    @Autowired
    private ReportDoanhThuRepository reportDoanhThuRepository;
    @Autowired
    private StorageService storageService;
    @Autowired private EntityManager entityManager;
    @Override
    public Optional<AdmMedical> save(AdmMedical entity) {
        return Optional.of(storageService.save(admMedicalRepository, entity));
    }

    @Override
    public Optional<AdmMedical> update(AdmMedical form) throws BadRequestException {
        AdmMedical bo = storageService.getById(admMedicalRepository, form.getId());
//        return Optional.of();
        AdmMedical res = storageService.update(admMedicalRepository, bo.formToBo(form, bo));
        if (H.isTrue(res)) {
            return Optional.of(res);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AdmMedical> get(Long id) throws BadRequestException {
        AdmMedical bo = storageService.getById(admMedicalRepository, id);
        if (H.isTrue(bo)) {
            return Optional.of(bo);
        }
        return Optional.empty();
    }

    @Override
    public Page<AdmMedical> getPaging(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmMedical
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmMedical> lA = entityManager.createQuery("select u from AdmMedical u where 1=1 and u.isDelete = 0", AdmMedical.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmMedical> list = new ArrayList<>();
        String hql = " from AdmMedical u left join u.admAppointment a left join a.admDoctor ad_ " +
                "left join a.admStaff as_ left join a.admCustomer ac_  " +
                " left join a.services se_ "+
                " where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u ) ", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);


        if(H.isTrue(search.getKeyword())) {
            List<QueryBuilder.ConditionObject> conditionObjects1 = new ArrayList<>();
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.medicalCode", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.note", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.diagnostic", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"u.prescription", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"as_.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"ad_.fullName", "%" + search.getKeyword().trim() + "%"));
            conditionObjects1.add(new QueryBuilder.ConditionObject(QueryUtils.LIKE,"ac_.fullName", "%" + search.getKeyword().trim() + "%"));
            builder.andOrListNative(conditionObjects1);
        }

        //medicalCode
        if (H.isTrue(search.getMedicalCode())) {
            builder.and(QueryUtils.LIKE, "u.medicalCode", "%" + search.getMedicalCode().trim() + "%");
        }
        //appointmentCode
        if (H.isTrue(search.getAppointmentCode())) {
            builder.and(QueryUtils.LIKE, "a.appointmentCode", "%" + search.getAppointmentCode().trim() + "%");
        }
        // customerId
        if (H.isTrue(search.getCustomerId())) {
            builder.and(QueryUtils.EQ, "ac_.id", search.getCustomerId());
        }
        // doctorId
        if (H.isTrue(search.getDoctorId())) {
            builder.and(QueryUtils.EQ, "ad_.id", search.getDoctorId());
        }
        // serviceId
        if (H.isTrue(search.getServiceId())) {
            builder.and(QueryUtils.IN, "se_.id", search.getServiceId());
        }
        //medicalDateFrom and medicalDateTo
        if (H.isTrue(search.getMedicalDateFrom())) {
            builder.and(QueryUtils.GE, "u.medicalDate", search.getMedicalDateFrom());
        }
        if (H.isTrue(search.getMedicalDateTo())) {
            builder.and(QueryUtils.LE, "u.medicalDate", search.getMedicalDateTo());
        }
        // diagnostic: chẩn doán
        if (H.isTrue(search.getDiagnostic())) {
            builder.and(QueryUtils.LIKE, "u.diagnostic", "%" + search.getDiagnostic().trim() + "%");
        }
        //modifiedDateFrom and modifiedDateTo
        if (H.isTrue(search.getModifiedDateFrom())) {
            builder.and(QueryUtils.GE, "u.modifiedDate", search.getModifiedDateFrom());
        }
        if (H.isTrue(search.getModifiedDateTo())) {
            builder.and(QueryUtils.LE, "u.modifiedDate", search.getModifiedDateTo());
        }
        //modifiedBy
        if (H.isTrue(search.getModifiedBy())) {
            builder.and(QueryUtils.LIKE, "u.modifiedBy", "%" +search.getModifiedBy() + "%");
        }
        // status
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "a.status",  search.getStatus());
        }

        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmMedical.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }

    @Override
    public List<AdmMedical> getAll() {
        return admMedicalRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) throws BadRequestException {
        AdmMedical medical = storageService.getById(admMedicalRepository, id);
        if (H.isTrue(medical)) {
            medical.setIsDelete(Constants.IS_DELETE.DELETED);
            storageService.save(admMedicalRepository, medical);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteIds(List<Long> ids) {
        List<AdmMedical> medicals = storageService.getByIds(admMedicalRepository, ids).stream().map(item -> {
            item.setIsDelete(Constants.IS_DELETE.DELETED);
            return item;
        }).collect(Collectors.toList());
        storageService.update(admMedicalRepository, medicals);
        return true;
    }
}
