package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.contants.Constants;
import com.huynv.dentcareWebApi.dto.request.SearchForm;
import com.huynv.dentcareWebApi.entity.AdmService;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.entity.view.ViewDoanhThu1;
import com.huynv.dentcareWebApi.repository.AdmServiceRepository;
import com.huynv.dentcareWebApi.repository.AdmUserRepository;
import com.huynv.dentcareWebApi.service.AdmUserService;
import com.huynv.dentcareWebApi.service.ServiceService;
import com.huynv.dentcareWebApi.service.StorageService;
import com.huynv.dentcareWebApi.utils.H;
import com.huynv.dentcareWebApi.utils.QueryBuilder;
import com.huynv.dentcareWebApi.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class ServiceServiceImpl extends BaseServiceImpl<AdmService, AdmServiceRepository> implements ServiceService {
    public ServiceServiceImpl(AdmServiceRepository repository) {
        super(repository);
    }

    @Autowired private StorageService storageService;
    @Autowired private AdmServiceRepository admServiceRepository;
    @Autowired private EntityManager entityManager;
    @Override
    public Page getPage(SearchForm search, Pageable pageable) {
        Page page = null;
        //From AdmCustomer
        if (search.getTypeGet() == Constants.TYPE_GET.ALL) {
            List<AdmService> lA = entityManager.createQuery("select u from AdmService u where 1=1 and u.isDelete = 0", AdmService.class).getResultList();
            return new PageImpl<>(lA, PageRequest.of(0, lA.size()), lA.size());
        }
        List<AdmService> list = new ArrayList<>();
        String hql = " from AdmService u   " +
                "where 1=1 ";
        //lefrt
        QueryBuilder builder = new QueryBuilder(entityManager, "select count( distinct u)", new StringBuffer(hql), false);

        //IS_DELETED
        builder.and(QueryUtils.EQ, "u.isDelete", Constants.IS_DELETE.NOT_DELETED);

        List<QueryBuilder.ConditionObject> conditionObjects = new ArrayList<>();
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.ACTIVE));
        conditionObjects.add(new QueryBuilder.ConditionObject(QueryUtils.EQ, "u.status", Constants.STATUS.LOCK));
        builder.andOrListNative(conditionObjects);

//        if (H.isTrue(search.getUsername())) {
//            builder.and(QueryUtils.LIKE, "u.username", "%" + search.getDistrictName().trim() + "%");
//        }
        if (H.isTrue(search.getName())) {
            builder.and(QueryUtils.LIKE, "u.fullName", "%" + search.getName().trim() + "%");
        }
        //email
        if (H.isTrue(search.getType())) {
            builder.and(QueryUtils.LIKE, "u.type", search.getType());
        }
        if (H.isTrue(search.getStatus())) {
            builder.and(QueryUtils.EQ, "u.status", search.getStatus());
        }
        Query query = builder.initQuery(false);
        int count = Integer.parseInt(query.getSingleResult().toString());

        pageable.getSort().iterator().forEachRemaining(order -> {
            builder.addOrder("u." + order.getProperty(), order.getDirection().isAscending() ? "ASC" : "DESC");
        });
        builder.addOrder("u.createdDate", QueryUtils.DESC);

        builder.setSubFix("select distinct u");
        query = builder.initQuery(AdmService.class);
        if (pageable.getPageSize() > 0) {
            query.setFirstResult(Integer.parseInt(String.valueOf(pageable.getOffset()))).setMaxResults(pageable.getPageSize());
        }
        list = query.getResultList();

        if (list != null) {
            page = new PageImpl<>(list, pageable, count);
        }
        return page;
    }
    @Override
    public AdmService create(AdmService service) {
        return storageService.save(admServiceRepository, service);
    }

    @Override
    public Optional<AdmService> update(AdmService service) {
        return Optional.of(storageService.update(admServiceRepository, service));
    }

    @Override
    public boolean delete(List<Long> ids) {
        List<AdmService> services = storageService.getByIds(admServiceRepository, ids).stream().map(user -> {
            user.setIsDelete(Constants.IS_DELETE.DELETED);
            return user;
        }).collect(Collectors.toList());
        storageService.update(admServiceRepository, services);
        return true;
    }
}
