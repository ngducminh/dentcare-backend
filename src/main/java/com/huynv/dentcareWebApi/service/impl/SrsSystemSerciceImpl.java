package com.huynv.dentcareWebApi.service.impl;

import com.huynv.dentcareWebApi.dto.request.ISTokenInfo;
import com.huynv.dentcareWebApi.dto.request.LoginRequest;
import com.huynv.dentcareWebApi.dto.request.UserInfo;
import com.huynv.dentcareWebApi.entity.AdmUser;
import com.huynv.dentcareWebApi.exception.BadRequestException;
import com.huynv.dentcareWebApi.exception.UnauthorizedException;
import com.huynv.dentcareWebApi.security.TokenHelper;
import com.huynv.dentcareWebApi.service.SrsSystemSercice;
import com.huynv.dentcareWebApi.utils.UtilsCommon;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class SrsSystemSerciceImpl implements SrsSystemSercice {
    @Autowired
    private TokenHelper tokenHelper;
    @Autowired private MessageSource messageSource;
    @Autowired private AuthenticationManager authenticationManager;
    @Override
    public UserInfo loginJWT(LoginRequest loginRequest) throws UnauthorizedException, BadRequestException {

        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            String session = servletRequestAttributes.getSessionId();

            AdmUser user = (AdmUser) authentication.getPrincipal();

            List<String> listRole =user.getGrantedAuths().stream().map(auth -> auth.getAuthority()).collect(Collectors.toList());
//            if(!H.isTrue(listRole)) {
//                throw new UnauthorizedException(messageSource.getMessage("error_401", null, UtilsCommon.getLocale()));
//            }

            UserInfo userInfo = new UserInfo();
            // Trả về jwt cho người dùng.
            String jwt = tokenHelper.generateToken(loginRequest, user, session);
            ISTokenInfo accessToken = new ISTokenInfo();
            accessToken.setAccessToken(jwt);
            accessToken.setTokenType("Bearer");
            accessToken.setExpiresIn(tokenHelper.getClaimsFromToken(jwt).getExpiration().getTime());
            userInfo.setAccessTokenInfo(accessToken);
            userInfo.setUsername(user.getUsername());
//            userInfo.setMobileAlias(user.getPhone());
//            userInfo.setEmail(user.getEmail());
            userInfo.setAuthorities(listRole);
            return userInfo;
        } catch (BadCredentialsException e) {
            log.error(loginRequest.getUsername() + "|" + e.getMessage());
            Locale locale = LocaleContextHolder.getLocale();
            LocaleContextHolder.setDefaultLocale(locale);
            Locale locale1 = LocaleContextHolder.getLocaleContext().getLocale();
            String msg = messageSource.getMessage("MSG_LOGIN_FAILED", null, locale);
            throw new BadRequestException(msg);
        } catch (AccessDeniedException ex1) {
            log.error(loginRequest.getUsername() + "|" + ex1.getMessage());
            throw new BadRequestException(messageSource.getMessage("error_401", null, UtilsCommon.getLocale()));
        } catch (LockedException ex) {
            log.error(loginRequest.getUsername() + "|" + ex.getMessage());
            throw new BadRequestException(messageSource.getMessage("MSG_LOCKED_ACCOUNT", null, UtilsCommon.getLocale()));
        } catch (DisabledException exx) {
            log.error(loginRequest.getUsername() + "|" + exx.getMessage());
            throw new BadRequestException(messageSource.getMessage("MSG_DISABLE_ACCOUNT", null, UtilsCommon.getLocale()));
        } catch (AuthenticationException atx) {
            log.error(loginRequest.getUsername() + "|" + atx.getMessage());
            throw new BadRequestException(messageSource.getMessage("NOTE_EVICT", null, UtilsCommon.getLocale()));
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException ex) {
            log.error(loginRequest.getUsername() + "|" + ex.getMessage());
            throw new BadRequestException(messageSource.getMessage("error.login_fail", null, UtilsCommon.getLocale()));
        }
    }
}
