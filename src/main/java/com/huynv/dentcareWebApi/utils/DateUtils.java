package com.huynv.dentcareWebApi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    private final static String templateFormatDefault = "yyyy-MM-dd";
    private final static String templateFormatFull = "yyyy-MM-dd HH:mm:ss";
    private final static String templateFormatDefaultV2 = "dd-MM-yyyy HH:mm:ss";
    private final static String templateFormatFullReverse = "HH:mm:ss dd-MM-yyyy";
    private final static String templateFormat = "dd-MM-yyyy";

    public static String present() {
        SimpleDateFormat format = new SimpleDateFormat(templateFormat);
        return format.format(new Date());
    }

    public static Date unixToDate(Long unix) {
        return new Date(unix*1000);
    }
    public static Date toDate(String input) throws ParseException {
        SimpleDateFormat formatDefault = new SimpleDateFormat(templateFormatDefault);
        return formatDefault.parse(input);
    }

    public static Date toDateReverse(String input) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(templateFormat);
        return format.parse(input);
    }

    public static Date toDateFull(String input) throws ParseException {
        SimpleDateFormat formatFull = new SimpleDateFormat(templateFormatFull);
        return formatFull.parse(input);
    }

    public static String dateToString(Date date) {
        SimpleDateFormat formatDefaultV2 = new SimpleDateFormat(templateFormatDefaultV2);
        return formatDefaultV2.format(date);
    }

    public static String toDateFull(Date input) throws ParseException {
        SimpleDateFormat formatFullReverse = new SimpleDateFormat(templateFormatFullReverse);
        return formatFullReverse.format(input);
    }

    public static String convertFormat(String input, String inputFormat, String outputFormat) {
        if (input == null) return null;
        try {
            SimpleDateFormat inputSdf = new SimpleDateFormat(inputFormat);
            Date date = inputSdf.parse(input);
            SimpleDateFormat outputSdf = new SimpleDateFormat(outputFormat);
            return outputSdf.format(date);
        } catch (ParseException pe) {
            return null;
        }
    }

    public static String yesterday() {
        Calendar calendar = Calendar.getInstance();
//    calendar.roll(Calendar.DATE, -1);
        SimpleDateFormat formatDefault = new SimpleDateFormat(templateFormatDefault);
        return formatDefault.format(calendar.getTime());
    }


    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static void main(String[] args) {
        System.out.println(atStartOfDay(new Date()));
        System.out.println(atEndOfDay(new Date()));
    }

    public static String convertDateToStringWithType(Date fromDate, String type) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(type);
        return simpleDateFormat.format(fromDate);
    }
}
