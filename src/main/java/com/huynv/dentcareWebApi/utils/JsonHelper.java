package com.huynv.dentcareWebApi.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonHelper.class);

    public static <T> T jsonToObject(String json, Class<T> classOfT) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, classOfT);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

}
