package com.huynv.dentcareWebApi.utils;

import com.huynv.dentcareWebApi.entity.AdmUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Optional;


/**
 * TODO: write you class description here
 *
 * @author
 */

public class UtilsCommon {

    public static Locale getLocale() {
//        try {
//            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//            return request.getLocale();
//        } catch (Exception e) {
//            return Locale.getDefault();
//        }
        //return default locale is vi-VN
        return new Locale("vi", "VN");
    }

//    public static String getEmailLogin() {
//        try {
//            AdmUser authentication = (AdmUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            return authentication.getEmail();
//        } catch (Exception e) {
//            return null;
//        }
//    }

    public static Optional<AdmUser> getUserLogin() {
        try {
            if (getAuthentication() == null)
                return Optional.empty();
            AdmUser user = (AdmUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return Optional.of(user);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static Authentication getAuthentication() {
        try {
            return SecurityContextHolder.getContext().getAuthentication();
        } catch (Exception e) {
            return null;
        }
    }

    public static void addSession(String key, Object value) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(true);
        session.setAttribute(key, value);
    }

    public static HttpSession sessionContext() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }

    public static Object getSessionKey(String key) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true).getAttribute(key);
    }

}
