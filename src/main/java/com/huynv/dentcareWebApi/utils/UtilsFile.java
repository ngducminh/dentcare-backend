package com.huynv.dentcareWebApi.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UtilsFile {

    private static final String SUFFIX = ".dat";
    private static final String SUFFIX_DOC = ".doc";
    private static final String SUFFIX_DOCX = ".docx";

    public static Set<String> listFilesUsingFilesList(String dir) throws IOException {
        try (Stream<Path> stream = Files.list(Paths.get(dir))) {
            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        }
    }

    public static void main(String[] args) throws IOException {
        /*Set<String> collect = listFilesUsingFilesList("C:/project/_upload");
        System.out.println(1);*/

        try(BufferedReader br = new BufferedReader(new FileReader("C:/project/OSP/SO_HOA/api/app-core/src/main/java/com/osp/core/config/text.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
        }
    }

    public static void saveFile(MultipartFile multipartFile, String pathFile) throws IOException {
        Files.copy(multipartFile.getInputStream(), Paths.get(pathFile), StandardCopyOption.REPLACE_EXISTING);
    }

    public static boolean checkFileExit(String path) {
        File file = new File(path);
        return file.exists();
    }

    public static void createFolder(String directory) {
        File dirs = new File(directory);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
    }

    public static File createNewFileWithOrigName(String directory, String fileName) {
        File dirs = new File(directory);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        return new File(directory+fileName);
    }

    // đọc nội dung file
    public String getContentFile(String path) {
        try {
            File file = new File(path);
            InputStream inputStream = new FileInputStream(file);
            String data = readFromInputStream(inputStream);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    private String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        inputStream.close();
        return resultStringBuilder.toString();
    }

    public static void deleteLogFile() {
        File logFolder = new File("/web/log");
        if (!logFolder.exists()) {
            return;
        }

        FileFilter ff = new FileFilter() {

            public boolean accept(File f) {
                //Loc file qua 10 ngay
                if (Calendar.getInstance().getTimeInMillis() - f.lastModified() > 864000000) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        File[] logFiles = logFolder.listFiles(ff);

        for (File file : logFiles) {
            file.delete();
        }
    }
}
